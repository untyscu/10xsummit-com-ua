<?php header('Access-Control-Allow-Origin: *'); ?>
<?php
//echo $_SERVER['HTTP_HOST'];
$get = '';
if(isset($_SERVER['REQUEST_URI']) && $_SERVER['REQUEST_URI'] != ''){
    $get = $_SERVER['REQUEST_URI'];
}
$z = array_reverse(explode('.', $_SERVER['HTTP_HOST']));
$country = geoip_country_code_by_name($_SERVER['REMOTE_ADDR']);

if($z[0] == 'ua' && $country == 'RU'){
	#echo '1';
	header('Location: http://10xsummit.ru'.$get);
}

if($z[0] == 'ru' && $country != 'RU'){
	#echo '2';
	header('Location: http://10xsummit.com.ua'.$get);
}

?>
<!DOCTYPE html>
<html lang="ru">
	<head>
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<title>10XСАММИТ - Бизнес-Событие №1 в РФ и СНГ по контенту</title>
		<!--Optimize-->
		<style>.async-hide { opacity: 0 !important} </style>
		<script>(function(a,s,y,n,c,h,i,d,e){s.className+=' '+y;h.start=1*new Date;
		h.end=i=function(){s.className=s.className.replace(RegExp(' ?'+y),'')};
		(a[n]=a[n]||[]).hide=h;setTimeout(function(){i();h.end=null},c);h.timeout=c;
		})(window,document.documentElement,'async-hide','dataLayer',4000,
		{'GTM-WJ3GRB5':true});</script>
		<!--Analytic-->
		<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
		ga('create', 'UA-38087671-1', 'auto');
		ga('require', 'GTM-WJ3GRB5');
		ga('send', 'pageview');
		</script>
		<!--Tag manager-->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-TZFD9SP');</script>
		<!-- End Google Tag Manager -->
        <meta name="description" content="Современные бизнес-технологии 10X от прогрессивных предпринимателей и топовых экспертов для кратного роста в 2019 году!">
		<meta name="keywords" content="" />
		<link rel="stylesheet" href="../src/css/style.min.css?v1.5" type="text/css"  media="all" />

        <!--for Facebook-->
        <meta property="og:url"           content="http://10xsummit.com.ua/" />
        <meta property="og:type"          content="website" />
        <meta property="og:title"         content="Бизнес-конференция No1 в Украине, РФ и СНГ по контенту" />
        <meta property="og:description"   content="Современные бизнес-технологии 10X от прогрессивных предпринимателей и топовых экспертов для кратного роста в 2019 году!" />
        <meta property="og:image"         content="../src/img/opengraph/fb.png" />
        <meta property="og:image:width"   content="1200" />
        <meta property="og:image:height"  content="628" />
        <!-- <meta property="og:vk:image"   content="../src/img/opengraph/vk.png" />
        <meta property="twitter:image" content="../src/img/opengraph/vk.png" /> -->
        <meta property="vk:image"      content="../src/img/opengraph/vk.png" />
        <!--/for Facebook-->

		<link rel="apple-touch-icon" sizes="57x57" href="../favicon/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="../favicon/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="../favicon/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="../favicon/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="../favicon/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="../favicon/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="../favicon/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="../favicon/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="../favicon/apple-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="192x192"  href="../favicon/android-icon-192x192.png">
		<link rel="icon" type="image/png" sizes="32x32" href="../favicon/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="96x96" href="../favicon/favicon-96x96.png">
		<link rel="icon" type="image/png" sizes="16x16" href="../favicon/favicon-16x16.png">
		<link rel="manifest" href="../favicon/manifest.json">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="favicon/ms-icon-144x144.png">
		<meta name="theme-color" content="#ffffff">
		<script type="text/javascript" src="../js/share.js?93" charset="windows-1251"></script>
	</head>
	<body>
		<!-- Google Tag Manager (noscript) -->
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TZFD9SP"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->
		<!--fb share-
		<div id="fb-root">
		</div>
		<script>(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = 'https://connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v3.1';
		fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>
		!--/fb share-->
		<nav class="topnav" id="topNav">
			<div class="container row">
				<a href="#top" class="topnav__logo">10x&nbsp;Саммит&nbsp;2018</a>
				<div class="topnav__menu-cover"></div>
				<div class="topnav__time tooltip">
					<span class="tooltip__icon">
						<i class="icon-clock"></i>
					</span>
					<span class="tooltip__text-bottom">
						На связи: Пн. - Пт.,  9:00 - 18:00 [МСК]
					</span>
				</div>
				<div class="topnav__phones">
					<p class="phones-ru">+7  (495) 133-82-24</p>
					<p class="phones-ua">+38  (044) 221-32-24</p>
				</div>
			</div>
		</nav>
		<section class="thanks-page">
			<div class="thanks-header">
				<span class="header__crystal-1"></span>
				<span class="header__crystal-2"></span>
				<span class="header__crystal-3"></span>
				<span class="header__crystal-4"></span>
				<h1 class="thanks-header__title scrollreveal">
				<span class="thanks-header__title__date"><b>Отлично!</b></span>
				</h1>
				 <p class="thanks-header__intro">Мы свяжемся с вами в ближайшее время, чтобы предоставить групповую скидку.</p>
				 <p><b>Наш рабочий график:</b></p>
				 <p>ПН-ПТ, 9:00-18:00 (МСК)</p>
				 <a href="../index.php" style="color:#fff;">Вернутся на главную</a>
            </div>
		</section>
		<section class="prefooter">
			<div class="container row">
				<div class="prefooter_call">
					<p><b>Ответим</b> на ваши вопросы:</p>
					<p>
						<a href="mailto:support@goldcoach.ru">
							<i class="icon-f_env"></i>
							<span>support@goldcoach.ru</span>
						</a>
					</p>
					<div>
						<a href="http://m.me/goldcoach.ru/" target="_blank">
							<i class="icon-chat_fb"></i>
						</a>
						<a href="https://vk.me/-168188452" target="_blank">
							<i class="icon-chat_vk"></i>
						</a>
						<a href="viber://pa?chatURI=goldcoach" target="_blank">
							<i class="icon-chat_w"></i>
						</a>
						<a href="https://t.me/goldcoachbot" target="_blank">
							<i class="icon-chat_telega"></i>
						</a>
					</div>
					<div>
						<div class="tooltip prefooter__tooltip">
							<span class="tooltip__icon btn-orange2">
								Заказать звонок
							</span>
							<span class="tooltip__text-bottom">
								
								<form class="form-noreload"  action="https://forms.ontraport.com/v2.4/form_processor.php?" method="post" accept-charset="UTF-8">
                                
								    Введите свой телефон - мы перевзоним вам в ближайшее время.
									<input name="utmspecial_428" type="hidden" value="<?=isset($_GET['utm_special'])?$_GET['utm_special']:''?>"/>
									<input name="utmcampaig_224" type="hidden" value="<?=isset($_GET['utm_campaign'])?$_GET['utm_campaign']:''?>"/>
									<input name="utmmedium_225" type="hidden" value="<?=isset($_GET['utm_medium']) ? $_GET['utm_medium']:''?>"/>
									<input name="utmsource_223" type="hidden" value="<?=isset($_GET['utm_source']) ? $_GET['utm_source']:''?>"/>
									<input name="afft_" type="hidden" value=""/>
									<input name="aff_" type="hidden" value=""/>
									<input name="sess_" type="hidden" value=""/>
									<input name="ref_" type="hidden" value=""/>
									<input name="own_" type="hidden" value=""/>
									<input name="oprid" type="hidden" value=""/>
									<input name="contact_id" type="hidden" value=""/>
									<input name="utm_source" type="hidden" value="<?=isset($_GET['utm_source']) ? $_GET['utm_source']:''?>"/>
									<input name="utm_medium" type="hidden" value="<?=isset($_GET['utm_medium']) ? $_GET['utm_medium']:''?>"/>
									<input name="utm_term" type="hidden" value="<?=isset($_GET['utm_term']) ? $_GET['utm_term']:''?>"/>
									<input name="utm_content" type="hidden" value="<?=isset($_GET['utm_content']) ? $_GET['utm_content']:''?>"/>
									<input name="utm_campaign" type="hidden" value="<?=isset($_GET['utm_campaign']) ? $_GET['utm_campaign']:''?>"/>
									<input name="referral_page" type="hidden" value=""/>
									<input name="uid" type="hidden" value="p2c9448f769"/>
									<input name="mopsbbk" type="hidden" value="617ED3B5A887895493C1765D:FFDD7C4EC4A053D5ED0145CE"/>
									<input name="cell_phone" required type="text" class="big-input" value="" placeholder="Ваш номер телефона"/>
									<div class="moonray-form-element-wrapper moonray-form-element-wrapper-alignment-left moonray-form-input-type-submit">
										<input type="submit" name="submit-button" value="отправить" class="btn-orange2" id="mr-field-element-584743536048"/>
									</div>
								</form>
								<span class="form-thanks">
                                    <b>Заявку приняли.<br>Ожидайте звонка в ближайшее время!</b>
                                    <br>
                                    <hr>
                                    Мы работаем: ПН-ПТ, 9:00 - 18:00 МСК
                                </span>
							</span>
						</div>
					</div>
				</div>
				<div class="prefooter_follow">
					<p>Подписывайтесь на каналы организатора и следите за новостями по саммиту</p>
					<div class="justify">
						<a href="https://www.facebook.com/zimbitsky" target="_blank" rel="nofollow">
							<i class="icon-fb_r"></i>
						</a>
						<a href="https://vk.com/ivanzimbitskiy" target="_blank" rel="nofollow">
							<i class="icon-vk_r"></i>
						</a>
						<a href="https://instagram.com/izimbitskiy/" target="_blank" rel="nofollow">
							<i class="icon-insta_r"></i>
						</a>
						<a href="https://www.youtube.com/user/izimbitskiy" target="_blank" rel="nofollow">
							<i class="icon-youtube_r"></i>
						</a>
						<a href="https://t.me/zimbitskiy" target="_blank" rel="nofollow">
							<i class="icon-telega_r"></i>
						</a>
					</div>
				</div>
				<div class="prefooter_share">
					<p>Таким событием стоит поделиться с друзьями:</p>
					<div class="center">
						<div id="vk_share">
                        <a href="https://vk.com/share.php?url=http://10xsummit.ru/&image=http://10xsummit.ru/src/img/opengraph/vk.png&title=%D0%91%D0%B8%D0%B7%D0%BD%D0%B5%D1%81-%D0%BA%D0%BE%D0%BD%D1%84%D0%B5%D1%80%D0%B5%D0%BD%D1%86%D0%B8%D1%8F%20%E2%84%961%20%D0%B2%20%D0%A0%D0%A4%20%D0%B8%20%D0%A1%D0%9D%D0%93%20%D0%BF%D0%BE%20%D0%BA%D0%BE%D0%BD%D1%82%D0%B5%D0%BD%D1%82%D1%83&description=%D0%A1%D0%BE%D0%B2%D1%80%D0%B5%D0%BC%D0%B5%D0%BD%D0%BD%D1%8B%D0%B5%20%D0%B1%D0%B8%D0%B7%D0%BD%D0%B5%D1%81-%D1%82%D0%B5%D1%85%D0%BD%D0%BE%D0%BB%D0%BE%D0%B3%D0%B8%D0%B8%2010X%20%D0%BE%D1%82%20%D0%BF%D1%80%D0%BE%D0%B3%D1%80%D0%B5%D1%81%D1%81%D0%B8%D0%B2%D0%BD%D1%8B%D1%85%20%D0%BF%D1%80%D0%B5%D0%B4%D0%BF%D1%80%D0%B8%D0%BD%D0%B8%D0%BC%D0%B0%D1%82%D0%B5%D0%BB%D0%B5%D0%B9%20%D0%B8%20%D1%82%D0%BE%D0%BF%D0%BE%D0%B2%D1%8B%D1%85%20%D1%8D%D0%BA%D1%81%D0%BF%D0%B5%D1%80%D1%82%D0%BE%D0%B2%20%D0%B4%D0%BB%D1%8F%20%D0%BA%D1%80%D0%B0%D1%82%D0%BD%D0%BE%D0%B3%D0%BE%20%D1%80%D0%BE%D1%81%D1%82%D0%B0%20%D0%B2%202019%20%D0%B3%D0%BE%D0%B4%D1%83!" target="_blank"></a>
                    </div>

                    <!--https://developers.facebook.com/docs/plugins/share-button?locale=ru_RU-->
                    <div class="fb-share-button" data-href="http://10xsummit.ru/" data-layout="button" data-size="large" data-mobile-iframe="false"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2F10xsummit.ru" class="fb-xfbml-parse-ignore">
                        <i class="icon-fb"></i>
                    </a></div>
					</div>
				</div>
			</div>
		</section>
		<footer class="footer">
            <div class="container row">
                <div class="footer_col">
                    <p>
                        <a href="/" class="footer__logo">
                            <img src="../src/img/footer/logo.png" alt="10x&nbsp;Саммит">
                        </a>
                    </p>
                    <p class="mobile-none"><a href="">Политика конфиденциальности</a></p>
                    <p class="mobile-none">© ColdCoach.ru 2011 - 2018</p>
                </div>
                <div class="footer_col">
                    <p><a href="#regform_partner" class="open-modal">Стать партнером саммита</a></p>
                    <p><a href="#regform_sponsor" class="open-modal">Стать спонсором</a></p>
                </div>
                <div class="footer_col">
                    <p>Разработка и продвижение</p>
                    <p><a class="footer__dev" href="https://5xprofit.ru/?utm_source=10xsummit" target="_blank"><img src="../src/img/footer/create_logo.png" alt="" style="width: 175px; height: 40px;"></a></p>
                </div>
                <div class="footer_col footer_mobile-show">
                    <p><a href="">Политика конфиденциальности</a></p>
                    <p>© ColdCoach.ru 2011 - 2018</p>
                </div>
            </div>
        </footer>
		<!--modal forms-->
		<div class="modal__fon" id="modalFon">
		</div>
		<!--Partners-->
		<div class="modal modal__partners" id="regform_partner">
			<div class="text">
				<span class="close-modal">x</span>
				<div class="modal__title">
					<div class="modal__title-text">
						<strong>Присоединяйтесь</strong> к саммиту как партнер!
					</div>
				</div>
				<form class="form-noreload"  action="https://forms.ontraport.com/v2.4/form_processor.php?" method="post" accept-charset="UTF-8">
					<p>Если вы хотите стать информационным
					партнёром саммита, заполните форму: </p>
					<input name="firstname" type="text"  required value="" placeholder="Ваше имя" class="big-input"/>
					<input name="email" type="email" required value="" placeholder="Ваш e-mail" class="big-input"/>
					<input name="cell_phone" required type="text" value="" placeholder="Ваш мобильный телефон" class="big-input"/>
					<input name="company" type="text" placeholder="Компания" class="big-input"/>
					<input name="utmspecial_428" type="hidden" value="<?=isset($_GET['utm_special']) ? $_GET['utm_special']:''?>"/>
					<input name="utmcampaig_224" type="hidden" value="<?=isset($_GET['utm_campaign']) ? $_GET['utm_campaign']:''?>"/>
					<input name="utmmedium_225" type="hidden" value="<?=isset($_GET['utm_medium']) ? $_GET['utm_medium']:''?>"/>
					<input name="utmsource_223" type="hidden" value="<?=isset($_GET['utm_source']) ? $_GET['utm_source']:''?>"/>
					<div class="moonray-form-element-wrapper moonray-form-element-wrapper-alignment-left moonray-form-input-type-submit">
						<button type="submit" name="submit-button" value="узнать о рекламных возможностях" class="btn-orange3" id="mr-field-element-584743536048">Стать партнёром</button>
					</div>
					<input name="afft_" type="hidden" value=""/>
					<input name="aff_" type="hidden" value=""/>
					<input name="sess_" type="hidden" value=""/>
					<input name="ref_" type="hidden" value=""/>
					<input name="own_" type="hidden" value=""/>
					<input name="oprid" type="hidden" value=""/>
					<input name="contact_id" type="hidden" value=""/>
					<input name="utm_source" type="hidden" value="<?=isset($_GET['utm_source']) ? $_GET['utm_source']:''?>"/>
					<input name="utm_medium" type="hidden" value="<?=isset($_GET['utm_medium']) ? $_GET['utm_medium']:''?>"/>
					<input name="utm_term" type="hidden" value=""/>
					<input name="utm_content" type="hidden" value="<?=isset($_GET['utm_content']) ? $_GET['utm_content']:''?>"/>
					<input name="utm_campaign" type="hidden" value="<?=isset($_GET['utm_campaign']) ? $_GET['utm_campaign']:''?>"/>
					<input name="referral_page" type="hidden" value=""/>
					<input name="uid" type="hidden" value="p2c9448f772"/>
					<input name="mopsbbk" type="hidden" value="F254189B844FE80C5F46A499:951A4F0120031D4E3312B78D"/>
				</form>
				<div class="form-thanks">
                    <h2>Поздравляем с умным решением! </h2>
                    <p>Ожидайте звонка в течение дня в рабочее время.
                    </p>
                    <p>Мы работаем: ПН - ПТ, 9:00 - 18:00 (МСК)</p>
                </div>
			</div>
		</div>
		<!--Sponsors-->
		<div class="modal modal__partners" id="regform_sponsor">
			<div class="text">
				<span class="close-modal">x</span>
				<div class="modal__title">
					<div class="modal__title-text">
						<strong>Присоединяйтесь</strong> к саммиту как спонсор для усиления своего бренда и потока клиентов
					</div>
				</div>
				<form class="form-noreload"  action="https://forms.ontraport.com/v2.4/form_processor.php?" method="post" accept-charset="UTF-8">
					<p>10X САММИТ &mdash; это возможность получить поток самых качественных клиентов в кратчайшие сроки и укрепить свой бренд в головах наиболее активной аудитории предпринимателей, топ-менеджеров Украины и России, готовых инвестировать деньги в развитие себя и компании.</p>
					<input name="firstname" type="text"  required value="" placeholder="Ваше имя" class="big-input"/>
					<input name="email" type="email" required value="" placeholder="Ваш e-mail" class="big-input"/>
					<input name="cell_phone" required type="text" value="" placeholder="Ваш мобильный телефон" class="big-input"/>
					<input name="company" type="text" placeholder="Компания" class="big-input"/>
					<input name="utmspecial_428" type="hidden" value="<?=isset($_GET['utm_special']) ? $_GET['utm_special']:''?>"/>
					<input name="utmcampaig_224" type="hidden" value="<?=isset($_GET['utm_campaign']) ? $_GET['utm_campaign']:''?>"/>
					<input name="utmmedium_225" type="hidden" value="<?=isset($_GET['utm_medium']) ? $_GET['utm_medium']:''?>"/>
					<input name="utmsource_223" type="hidden" value="<?=isset($_GET['utm_source']) ? $_GET['utm_source']:''?>"/>
					<div class="moonray-form-element-wrapper moonray-form-element-wrapper-alignment-left moonray-form-input-type-submit">
						<button type="submit" name="submit-button" value="узнать о рекламных возможностях" class="btn-orange3" id="mr-field-element-584743536048">Стать спонсором</button>
					</div>
					<input name="afft_" type="hidden" value=""/>
					<input name="aff_" type="hidden" value=""/>
					<input name="sess_" type="hidden" value=""/>
					<input name="ref_" type="hidden" value=""/>
					<input name="own_" type="hidden" value=""/>
					<input name="oprid" type="hidden" value=""/>
					<input name="contact_id" type="hidden" value=""/>
					<input name="utm_source" type="hidden" value="<?=isset($_GET['utm_source']) ? $_GET['utm_source']:''?>"/>
					<input name="utm_medium" type="hidden" value="<?=isset($_GET['utm_medium']) ? $_GET['utm_medium']:''?>"/>
					<input name="utm_term" type="hidden" value=""/>
					<input name="utm_content" type="hidden" value="<?=isset($_GET['utm_content']) ? $_GET['utm_content']:''?>"/>
					<input name="utm_campaign" type="hidden" value="<?=isset($_GET['utm_campaign']) ? $_GET['utm_campaign']:''?>"/>
					<input name="referral_page" type="hidden" value=""/>
					<input name="uid" type="hidden" value="p2c9448f773"/>
					<input name="mopsbbk" type="hidden" value="72306761D47F11BBCFC2B2DC:110457A6F95D32C493F7C3FD"/>
				</form>
				<div class="form-thanks">
                    <h2>Поздравляем с умным решением! </h2>
                    <p>В ближайшее время мы свяжемся с вами 
                        и расскажем о рекламных возможностях.
                    </p>
                    <p>Мы работаем: ПН - ПТ, 9:00 - 18:00 (МСК)</p>
                </div>
			</div>
		</div>
		<!--Group-->
		<div class="modal modal__partners" id="regform_group">
			<div class="text">
				<span class="close-modal">x</span>
				<div class="modal__title">
					<div class="modal__title-text">
						Заберите групповую скидку на участие с друзьями
					</div>
				</div>
				<form class="form-noreload" action="https://forms.ontraport.com/v2.4/form_processor.php?" method="post" accept-charset="UTF-8">
					<p>Заполните форму, чтобы получить персональную скидку на билеты</p>
					<input name="firstname" type="text"  required value="" placeholder="Ваше имя" class="big-input"/>
					<input name="cell_phone" required type="text" value="" placeholder="Ваш телефон" class="big-input"/>
					<input name="company" type="text" placeholder="Сколько друзей берете?" class="big-input"/>
					<input name="utmspecial_428" type="hidden" value="<?=isset($_GET['utm_special']) ? $_GET['utm_special']:''?>"/>
					<input name="utmcampaig_224" type="hidden" value="<?=isset($_GET['utm_campaign']) ? $_GET['utm_campaign']:''?>"/>
					<input name="utmmedium_225" type="hidden" value="<?=isset($_GET['utm_medium']) ? $_GET['utm_medium']:''?>"/>
					<input name="utmsource_223" type="hidden" value="<?=isset($_GET['utm_source']) ? $_GET['utm_source']:''?>"/>
					<div class="moonray-form-element-wrapper moonray-form-element-wrapper-alignment-left moonray-form-input-type-submit">
						<button type="submit" name="submit-button" value="узнать о рекламных возможностях" class="btn-orange3" id="mr-field-element-584743536048"><span class="desktop-none">Забрать</span><span class="mobile-none">Получить групповую</span> скидку</button>
					</div>
					<input name="afft_" type="hidden" value=""/>
					<input name="aff_" type="hidden" value=""/>
					<input name="sess_" type="hidden" value=""/>
					<input name="ref_" type="hidden" value=""/>
					<input name="own_" type="hidden" value=""/>
					<input name="oprid" type="hidden" value=""/>
					<input name="contact_id" type="hidden" value=""/>
					<input name="utm_source" type="hidden" value="<?=isset($_GET['utm_source']) ? $_GET['utm_source']:''?>"/>
					<input name="utm_medium" type="hidden" value="<?=isset($_GET['utm_medium']) ? $_GET['utm_medium']:''?>"/>
					<input name="utm_term" type="hidden" value=""/>
					<input name="utm_content" type="hidden" value="<?=isset($_GET['utm_content']) ? $_GET['utm_content']:''?>"/>
					<input name="utm_campaign" type="hidden" value="<?=isset($_GET['utm_campaign']) ? $_GET['utm_campaign']:''?>"/>
					<input name="referral_page" type="hidden" value=""/>
					<input name="uid" type="hidden" value="p2c9448f773"/>
					<input name="mopsbbk" type="hidden" value="72306761D47F11BBCFC2B2DC:110457A6F95D32C493F7C3FD"/>
				</form>
				<div class="form-thanks">
                    <h2>Отлично!</h2>
                    <p>В ближайшее время мы свяжемся с вами 
                    и расскажем о рекламных возможностях. 
                    </p>
                    <p>Мы работаем: ПН - ПТ, 9:00 - 18:00 (МСК)</p>
                </div>
			</div>
		</div>
		<!--/modal forms-->
		<form style="display: none" id="payment-form" class="moonray-form-clearfix" action="https://forms.ontraport.com/v2.4/form_processor.php?" method="post" accept-charset="UTF-8">
			<input name="firstname" type="hidden" value="<?=isset($_GET['firstname'])?$_GET['firstname']:''?>" />
			<input name="email" type="hidden"  value="<?=isset($_GET['email'])?$_GET['email']:''?>" />
			<input name="cell_phone" required type="hidden" value="<?=isset($_GET['cell_phone'])?$_GET['cell_phone']:''?>" />
			<input name="utmspecial_428" type="hidden" value="<?=isset($_GET['utm_special'])?$_GET['utm_special']:''?>"/>
			<input name="utmcampaig_224" type="hidden" value="<?=isset($_GET['utm_campaign'])?$_GET['utm_campaign']:''?>"/>
			<input name="utmmedium_225" type="hidden" value="<?=isset($_GET['utm_medium'])?$_GET['']:'utm_medium'?>"/>
			<input name="utmsource_223" type="hidden" value="<?=isset($_GET['utm_source'])?$_GET['']:'utm_source'?>"/>
			<input type="submit" name="submit-button" value="Submit"/>
			<input name="afft_" type="hidden" value="<?=isset($_GET['afft_'])?$_GET['afft_']:''?>"/>
			<input name="aff_" type="hidden" value="<?=isset($_GET['aff_'])?$_GET['aff_']:''?>"/>
			<input name="sess_" type="hidden" value="<?=isset($_GET['sess_'])?$_GET['sess_']:''?>"/>
			<input name="ref_" type="hidden" value="<?=isset($_GET['ref_'])?$_GET['ref_']:''?>"/>
			<input name="own_" type="hidden" value="<?=isset($_GET['own_'])?$_GET['']:'own_'?>"/>
			<input name="oprid" type="hidden" value="<?=isset($_GET['oprid'])?$_GET['oprid']:''?>"/>
			<input name="contact_id" type="hidden" value="<?=isset($_GET['contact_id'])?$_GET['contact_id']:''?>"/>
			<input name="utm_source" type="hidden" value="<?=isset($_GET['utm_source'])?$_GET['']:'utm_source'?>"/>
			<input name="utm_medium" type="hidden" value="<?=isset($_GET['utm_medium'])?$_GET['utm_medium']:''?>"/>
			<input name="utm_term" type="hidden" value="<?=isset($_GET['utm_term'])?$_GET['']:'utm_term'?>"/>
			<input name="utm_content" type="hidden" value="<?=isset($_GET['utm_content'])?$_GET['utm_content']:''?>"/>
			<input name="utm_campaign" type="hidden" value="<?=isset($_GET['utm_campaign'])?$_GET['utm_campaign']:''?>"/>
			<input name="referral_page" type="hidden" value="<?=isset($_GET['referral_page'])?$_GET['referral_page']:''?>"/>
			<input name="uid" id="uid" type="hidden" value=""/>
			<input name="mopsbbk" id="mopsbbk" type="hidden" value=""/>
		</form>
		<script type="text/javascript" src="../src/js/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="../src/js/jquery-migrate-1.2.1.min.js"></script>
	<!--Roistat-->
	<script>(function(w, d, s, h, id) { w.roistatProjectId = id; w.roistatHost = h; var p = d.location.protocol == "https:" ? "https://" : "http://"; var u = /^.*roistat_visit=[^;]+(.*)?$/.test(d.cookie) ? "/dist/module.js" : "/api/site/1.0/"+id+"/init"; var js = d.createElement(s); js.charset="UTF-8"; js.async = 1; js.src = p+h+u; var js2 = d.getElementsByTagName(s)[0]; js2.parentNode.insertBefore(js, js2);})(window, document, 'script', 'cloud.roistat.com', '06bb8d989d90f9ec2816bc3debce9a5c');</script>
	<!--end Roistat-->

    <script type="text/javascript" src="../src/js/on-screen.min.js"></script>

    <script type="text/javascript" src="../src/js/scrollreveal.min.js"></script>
    <script>
        $(document).ready(function(){

        $('.payment-link').click(function(e){
            e.preventDefault();
            $('#mopsbbk').val($(this).attr('data-mopsbbk'));
            $('#uid').val($(this).attr('data-uid'));

            $('#payment-form').submit();
        });
            
        //share
        // document.getElementById('vk_share').innerHTML = VK.Share.button('http://mysite.com', {type: 'link', text : ' '});
            //animate on scroll
            $('a[href^="#"]:not(".open-modal")').on('click', function() {
                var target = $(this).attr('href');
        var position = $(target).offset().top;
        var body = $("html, body");
        if(target == '#price') {
            position += 200;
        }
        if(target == '#uniqueness') {
            position -= 100;
        }
        body.stop().animate({scrollTop:position}, '1000');
        return false;
            });
        });
         //TODO: submit form
            $('.form-noreload').on('submit', function(e) {
                e.preventDefault();
                e.stopPropagation();
                
                $(this).slideUp().next('.form-thanks').slideDown();

                return false;
            });
        //animate crystals
        $('.thanks-header').onScreen({
            doIn: function() {
            $('.thanks-header').addClass('animated');
            }
        });
        $('.price-block').onScreen({
            doOut: function() {
            $('.thanks-header').removeClass('animated');
            }
        });
        $('.corp-offer__offer .crystal-1').onScreen({
            doIn: function() {
            $('.corp-offer__offer').addClass('animated');
            }
        });
        $('.corp-offer__offer').onScreen({
            doOut: function() {
            $('.corp-offer__offer').removeClass('animated');
            }
        });
        //timeline
        var timeline_items = $('.timeline li');
        var now_date = new Date();
        timeline_items.each(function() {
            var start_date = Date.parse( $(this).data('time-start') );
            var end_date = Date.parse($(this).data('time-end') );
            var date_diff1,date_diff2;
            if(end_date <= now_date) {
                if($(this).hasClass('timeline__line')) {
                    $(this).find('.timeline__line-red').css('width', '100%');
                } else if($(this).hasClass('timeline__dot')) {
                    $(this).addClass('dot-orange');
                }
            } else if(start_date < now_date) {
                if($(this).hasClass('timeline__line')) {
                    date_diff1 = now_date - start_date;
                    date_diff2 = end_date - start_date;
                    $(this).find('.timeline__line-red').css('width', (date_diff1*100)/date_diff2+'%');
                } else if($(this).hasClass('timeline__dot')) {
                    $(this).addClass('dot-orange');
                }
            } else {
            }
        });
        //fix menu
        $(window).on('load scroll', function() {
    var scrollPoint = 81;
    var $topNav = $('#topNav');
    if( ($(window).scrollTop() > scrollPoint) && !$topNav.hasClass('fixed') ) {
    $topNav.addClass('fixed');
    } else if( ($(window).scrollTop() < scrollPoint) && $topNav.hasClass('fixed') ) {
    $topNav.removeClass('fixed');
    }
        });
        //modal
        $('.open-modal').on('click', function() {
            var modal_id = $(this).attr('href')+'';
            $('#modalFon').show();
            $(modal_id).css('top', (($(window).scrollTop() + 50) + 'px') );
            $(modal_id).show();
            return false;
        });
        $('.close-modal').on('click', function() {
            $('#modalFon').hide();
            $(this).parents('.modal').hide();
            return false;
        });
        // $('.checkbox__label').click(function() {
                //  alert('Без согласия на обработку данных мы не можем принять заявку!');
        // });
        //show blocks on scroll
        ScrollReveal().reveal('.scrollreveal');
        $('.menu-icon').click(function() {
            $('body').toggleClass('menu-open');
        });
        $('.phone-icon').click(function() {
            $('body').toggleClass('phone-open');
        });
    </script>
		<!-- Yandex.Metrika counter -->
		<script type="text/javascript" async>
		(function (d, w, c) {
		(w[c] = w[c] || []).push(function() {
		try {
		w.yaCounter39253290 = new Ya.Metrika2({
		id:39253290,
		clickmap:true,
		trackLinks:true,
		accurateTrackBounce:true,
		webvisor:true
		});
		} catch(e) { }
		});
		var n = d.getElementsByTagName("script")[0],
		s = d.createElement("script"),
		f = function () { n.parentNode.insertBefore(s, n); };
		s.type = "text/javascript";
		s.async = true;
		s.src = "https://mc.yandex.ru/metrika/tag.js";
		if (w.opera == "[object Opera]") {
		d.addEventListener("DOMContentLoaded", f, false);
		} else { f(); }
		})(document, window, "yandex_metrika_callbacks2");
		</script>
		<noscript>
		<div><img src="https://mc.yandex.ru/watch/39253290" style="position:absolute;
			left:-9999px;" alt="" />
		</div></noscript>
		<!-- /Yandex.Metrika counter -->
	</body>
</html>