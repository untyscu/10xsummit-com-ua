<?php
    $silver_price = 57;
    $gold_price = 77;
    $gold_pluse_price = 117;
    $vip_price = 397; 
    
    $silver_skidka = 54.15;
    $gold_skidka = 73.15;
    $gold_pluse_skidka = 111.15;
    $vip_skidka = 377.15;

if(strtotime(date("d-m-Y "))>strtotime("04-10-2018 ") || $_GET['mode'] == 'test'){
    $silver_price = 67;
    $gold_price = 87;
    $gold_pluse_price = 127;
    $vip_price = 437; 
    
    $silver_skidka = 63.65;
    $gold_skidka = 82.65;
    $gold_pluse_skidka = 120.65;
    $vip_skidka = 415.15; 
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Оплата билета на САММИТ 10Х</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css?ver='1.05'">
    <!--Optimize-->
    <style>.async-hide { opacity: 0 !important} </style>
    <script>(function(a,s,y,n,c,h,i,d,e){s.className+=' '+y;h.start=1*new Date;
    h.end=i=function(){s.className=s.className.replace(RegExp(' ?'+y),'')};
    (a[n]=a[n]||[]).hide=h;setTimeout(function(){i();h.end=null},c);h.timeout=c;
    })(window,document.documentElement,'async-hide','dataLayer',4000,
    {'GTM-WJ3GRB5':true});</script>
    <!--end Optimize-->
    <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-38087671-2"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-38087671-2');
        </script>
        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-PDFPNHM');</script>
        <!-- End Google Tag Manager -->
</head>
<body>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PDFPNHM"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <!-- Facebook Pixel Code -->
    <script>
      !function(f,b,e,v,n,t,s)
      {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
      n.callMethod.apply(n,arguments):n.queue.push(arguments)};
      if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
      n.queue=[];t=b.createElement(e);t.async=!0;
      t.src=v;s=b.getElementsByTagName(e)[0];
      s.parentNode.insertBefore(t,s)}(window, document,'script',
      'https://connect.facebook.net/en_US/fbevents.js');
      fbq('init', '1619239984767327');
      fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
      src="https://www.facebook.com/tr?id=1619239984767327&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Facebook Pixel Code --> 
    <div class="container">
        <div class="row">
            <div class="heder">
                <div class="main-heder">
                    <div class="logo"><img src="img\logotype.png" alt="Logotipe"></div>
                    <div class="contacts">
                        <div class="left">
                            <p>Остались вопросы? Звоните!</p>
                        </div>
                        <div class="right">
                            <div class="ru">
                                <p class="text-c">Россия:</p>
                                <p>+7 495 133 82 24</p>
                            </div>
                            <div class="uk">
                                <p class="text-c">Украина:</p>
                                <p>+38 096 916 13 88</p>
                            </div>
                            
                        </div>
                    </div>
                </div>
                <div class="caption">
                    <h1><b>Оплата билета на САММИТ 10Х</b></h1>
                    <p style="font-size: 26px;">Оплачивайте сейчас и получите скидку <span style="color: #ff6700; font-weight: 600">5%</span></p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="content">
                <div class="block-left">
                    <h2><b>На САММИТЕ 10Х получите<br>  протестированные технологии новой эры:</b></h2>
                    <ul>
                        <li><p>10Х ПРОДАЖИ.</p><p class="text-c">Инновационная (и уже много раз протестированная в разных сферах бизнеса) система продаж, которая помогает «закрывать» в 5-7 раз больше клиентов, чем ваши конкуренты сейчас. И вы наконец-то сможете инвестировать в рекламу в 2-5 раз больше, возвращая инвестиции почти мгновенно, масштабировать бизнес и развивая бренд.</p></li>
                        <li><p>10Х БРЕНД.</p><p class="text-c">Технология как методично перейти от небольшой группы «случайных» клиентов и подписчиков в соцсетях к масштабному сообществу единомышленников, которые доверяют вам, рассказывают о вас, рекомендуют вас и покупают у вас снова и снова с нулевым сопротивлением.</p></li>
                        <li><p>10Х ПОТОК.</p><p class="text-c">Самые перспективные каналы траффика и современные инструменты, которые дадут вам прогнозируемый 10-кратынй рост количества потенциальных клиентов в следующие 3-5 лет.</p></li>
                        <li><p>10Х КОМАНДА.</p><p class="text-c">Система как создать мотивированную команду из игроков А-класса, которая реализует любой, даже самый амбициозный план по щелчку пальца, не задавая лишних вопросов</p></li>
                    </ul>
                </div>
                <div class="block-right">
                    <div class="ticket">
                        <div class="for">
                            <p class="text-c">Билет на</p>
                            <p><b>"Саммит 10Х"</b></p>
                        </div>
                        <div class="date">
                            <p class="text-c">Дата: </p>
                            <p class="date">10-11 ноября</p>
                        </div>
                        <div class="member">
                            <p class="text-c">Участник:</p>
                            <h2><?=isset($_GET['firstname'])?$_GET['firstname']:''?></h2>
                        </div>
                        <div class="code">
                            <img src="img/code.png" alt="">
                        </div>
                    </div>
                    <div class="price">
                        <h2>Билет на грандиозный САММИТ 10Х</h2>
                        <p>Обычная цена - </p> <p class="text-c" style="text-decoration:line-through"><b>$<?=$gold_pluse_price;?></b></p>
                        <p>Цена со скидкой - </p> <p class="text-c"><b>$<?=$gold_pluse_skidka;?></b></p>
                    </div>
                    <div class="form">
                        <form action="oplata.php" method="POST">
                            <input type="email" class="input" placeholder="E-MAIL" name="email" value="<?=isset($_GET['email'])?$_GET['email']:''?>"><br>
                            <input type="submit" value="Оплатить картой">
                            <div class="link">
                                    <a href="https://goldcoach.ru/oplata/oplata.pdf">Другой способ оплаты</a><img src="img/icon-pdf.png" alt="PDF">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="foother">
                <p>02068, Украина, г. Киев, ул. Старонаводницкая, 6Б, оф.210<br>
                    Официальный адрес компании GoldCoach Ltd: Suite 1,4 Queen Street, Edinburgh, Scotland, EH21JE, United Kingdom</p>
                <ul>
                    <li><a href="http://goldcoach.ru/otkaz/" target="blank">Отказ от ответственности</a></li>
                    <li><a href="http://goldcoach.ru/agreement/" target="blank">Пользовательское соглашение</a></li>
                </ul>
            </div>
        </div>
    </div>
    
    <!-- Yandex.Metrika counter -->
    <script type="text/javascript" >
        (function (d, w, c) {
            (w[c] = w[c] || []).push(function() {
                try {
                    w.yaCounter39253290 = new Ya.Metrika2({
                        id:39253290,
                        clickmap:true,
                        trackLinks:true,
                        accurateTrackBounce:true,
                        webvisor:true
                    });
                } catch(e) { }
            });

            var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script"),
                f = function () { n.parentNode.insertBefore(s, n); };
            s.type = "text/javascript";
            s.async = true;
            s.src = "https://d31j93rd8oukbv.cloudfront.net/metrika/watch_ua.js";//"https://mc.yandex.ru/metrika/tag.js";

            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
            } else { f(); }
        })(document, window, "yandex_metrika_callbacks2");
    </script>
    <noscript><div><img src="https://mc.yandex.ru/watch/39253290" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!-- /Yandex.Metrika counter -->
    <!--Roistat-->
    <script>(function(w, d, s, h, id) { w.roistatProjectId = id; w.roistatHost = h; var p = d.location.protocol == "https:" ? "https://" : "http://"; var u = /^.*roistat_visit=[^;]+(.*)?$/.test(d.cookie) ? "/dist/module.js" : "/api/site/1.0/"+id+"/init"; var js = d.createElement(s); js.charset="UTF-8"; js.async = 1; js.src = p+h+u; var js2 = d.getElementsByTagName(s)[0]; js2.parentNode.insertBefore(js, js2);})(window, document, 'script', 'cloud.roistat.com', '06bb8d989d90f9ec2816bc3debce9a5c');</script>
    <!--end Roistat-->
    
    <!--Analytic-->
    <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-38087671-1', 'auto');
    ga('require', 'GTM-WJ3GRB5');
    ga('send', 'pageview');
    </script>
    <!--end Analytic-->
</body>
</html>