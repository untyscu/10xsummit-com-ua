<?php

$data = json_encode(
    [
        'transactionStatus' => 'Approved',
        'orderReference' => '73_12919249134134',
        'amount' => 39,
        'email' => 'serdiuk.oleksandr@gmail.com',
    ]
);

// getting stream
$ch = curl_init();

// setting request url
curl_setopt ($ch, CURLOPT_URL, 'https://goldcoach.ru/shop/wfp.php');

// Http POST method
curl_setopt ($ch, CURLOPT_POST, 1);

// Getting Invoice Request
curl_setopt ($ch, CURLOPT_POSTFIELDS, $data);

#curl_setopt($ch, CURLOPT_HEADER, true);
curl_setopt($ch, CURLOPT_HTTPHEADER,
    array(
        'Content-Type:application/json',
        'Content-Length: ' . strlen($data)
    )
);

// Return from server is required
curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);

// sending request to the server
$serverOutput = curl_exec ($ch);

// closing the stream
curl_close ($ch);

var_dump($serverOutput);