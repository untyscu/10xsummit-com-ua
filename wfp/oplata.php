<?php
/**
 * Created by PhpStorm.
 * User: Oleksandr Serdiuk
 * Date: 22.05.2017
 * Time: 10:57
 */

// getting server root
$server = $_SERVER['DOCUMENT_ROOT'];

// include request handler
include_once $server . '/wfp/Controllers/RequestController.php';

// setting up controller
$controller = new RequestController(73, 19, 'Marketing Plan Club', 'test@test.test', true);

// setting up request
$controller->determinate();