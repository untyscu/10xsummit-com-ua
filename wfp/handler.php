<?php

// getting server root
$server = $_SERVER['DOCUMENT_ROOT'];

// setting up test data
$email = $_GET['email'];
$productId = $_GET['productId'];
$productPrice = $_GET['productPrice'];
$productName = $_GET['productName'];
$regularType = $_GET['regularMode'];

if (!isset($regularType)) {
    $regularType = 'monthly';
}

// include request handler
include_once $server . '/wfp/Controllers/RequestController.php';

// setting up controller
$controller = new RequestController($productId, $productPrice, $productName, $email);
$collection = $controller->generateCollection();

echo '<form id="handler" method="post" action="https://secure.wayforpay.com/pay" style="display: none;">
            <input name="merchantAccount" value="' . $collection['merchantAccount'] . '">
            <input name="merchantAuthType" value="SimpleSignature">
            <input name="merchantDomainName" value="' . $collection['merchantDomainName'] . '">
            <input name="merchantSignature" value="' . $collection['merchantSignature'] . '">
            <input name="orderReference" value="' . $collection['orderReference'] . '">
            <input name="orderDate" value="' . $collection['orderDate'] . '">
            <input name="amount" value="' . $productPrice . '">
            <input name="currency" value="USD">
            <input name="orderTimeout" value="49000">
            <input name="productName[]" value="' . $productName . '">
            <input name="productPrice[]" value="' . $productPrice . '">
            <input name="productCount[]" value="1">
            <input name="clientEmail" value="' . $email . '">
            <input name="defaultPaymentSystem"="card">
            <input name="serviceUrl" value="' . $collection['serviceUrl'] . '" />
            
            <input name="regularMode" value="' . $regularType . '">
            <input name="regularAmount" value="' . $productPrice . '">
            <input name="dateNext" value="' . date('d.m.Y', strtotime('now +30 days')) . '">
            <input name="dateEnd" value="' . date('d.m.Y', strtotime('now +1 year')) . '">
            
            <button type="submit">Submit</button>
      </form>
      
      <script type="text/javascript">
          var form = document.getElementById(\'handler\');
          form.submit();
      </script>';
