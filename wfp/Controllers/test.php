<?php

include_once 'GetCourseController.php';

header('Content-Type: text/html; charset=utf-8');

// getting order object
$orderObject = explode('_', '66100_20170809205912');

$controller = new GetCourseController;
$controller->handle(array(
    'public_order_id' => $orderObject[1] . rand(1000, 10000),
    'productId' => $orderObject[0],
    'email' => 'serdiuk.oleksandr@gmail.com',
    'paymentSum' => '7',
));

echo 'success';