<?php

// include abstract handler
include_once 'AbstractRequestController.php';

/**
 * Created by PhpStorm.
 * User: Oleksandr Serdiuk
 * Date: 22.05.2017
 * Time: 10:45
 */
class RequestController extends AbstractRequestController
{
    /**
     * Handles redirects
     *
     * @var string
     */
    private $regularPaymentHandler = 'https://goldcoach.ru/wfp/handler.php';

    /**
     * Getting default regular mode
     *
     * @var string
     */
    const REGULAR_MODE = 'monthly';

    /**
     * Handles initialization of request
     *
     * RequestController constructor.
     * @param $productId
     * @param $productPrice
     * @param $productName
     * @param $email
     * @param $regularPayment
     */
    public function __construct($productId, $productPrice, $productName, $email, $regularPayment = false, $currency = self::PAYMENT_CURRENCY)
    {
        // parent call
        parent::__construct();

        // initialize request fields
        $this->requestFields = [
            'productId' => $productId,
            'orderId' => $productId . '_' . date('YmdHis'),
            'productPrice' => $productPrice,
            'productName' => $productName,
            'email' => $email,
            'regularPayment' => $regularPayment,
            'regularMode' => self::REGULAR_MODE,
        ];
				
				$this->invoiceRequestArray['currency'] = $currency;

        if (isset($_GET['regularMode'])) {
            $this->requestFields['regularMode'] = $_GET['regularMode'];
        }
    }

    /**
     * Collecting All parameters for
     * Payment System Request for Invoice Payment
     *
     * @return void
     */
    public function actionGenerateInvoiceRequest()
    {
        // handles initialization of basic order request
        $orderRequest = [
            'name' => '',
            'email' => $this->requestFields['email'],
            'phone' => '',

            'public_order_id' => $this->requestFields['orderId'],
            'order_complete_timestamp' => strtotime('now'),
            'order_total_sum' => $this->requestFields['productPrice'],
            'order_products_names' => array($this->requestFields['productName']),
            'order_products_prices' => array($this->requestFields['productPrice']),
            'order_products_count' => array(1),
        ];

        // collecting user information
        // for request
        $userCollection = array(
            'clientEmail' => $orderRequest['email'],
        );

        // if found name
        if ($orderRequest['name'] != '') {
            // setting up name
            $userCollection['clientFirstName'] = $orderRequest['name'];
        }

        // if found phone
        if ($orderRequest['phone'] != '') {
            // setting up phone
            $userCollection['clientPhone'] = $orderRequest['phone'];
        }

        // collecting order information
        $orderCollection = array(
            'orderReference' => $orderRequest['public_order_id'],
            'orderDate' => $orderRequest['order_complete_timestamp'],
            'amount' => $orderRequest['order_total_sum'],
            'productName' => $orderRequest['order_products_names'],
            'productPrice' => $orderRequest['order_products_prices'],
            'productCount' => $orderRequest['order_products_count'],
        );

        // if is regular payment
        if ($this->requestFields['regularPayment']) {
            // every month
            $orderCollection['regularMode'] = $this->requestFields['regularMode'];

            // pushing price
            $orderCollection['regularAmount'] = $this->requestFields['productPrice'];

            // setting up start with in end dates
            $orderCollection['dateNext'] = date('d.m.Y', strtotime('now +30 days'));
            $orderCollection['dateEnd'] = date('d.m.Y', strtotime('now +2 years'));
        }

        // merging user information array with order
        $requestArray = array_merge($userCollection, $orderCollection);

        // merging request information with available data
        $requestArray = $this->actionMergePaymentSystemArray($requestArray);

        $this->actionPushCustomFieldsForGenerationOfSignature($orderRequest);
        $this->actionGenerateMerchantSignature($orderRequest, $requestArray);

        // setting request
        $this->setRequest($requestArray);

        // returning curl response
        $this->actionMakeInvoiceRequest();
    }

    /**
     * Handles determination of request data with in a link
     */
    public function determinate()
    {
        // getting two symbol country shortcut
        $country = geoip_country_code_by_name($_SERVER['REMOTE_ADDR']);//$_SERVER['HTTP_CF_IPCOUNTRY'];

        // if not Ukraine request
        if ($country != 'UA') {
            // getting rur curs
            $curs = file_get_contents('http://call-center.extremehealth.ru/core/class/currency.api.php');
            // getting price
            $price_rur = $this->requestFields['productPrice'] * $curs;

            /** @var  $shopId */
            $shopId = '137380';
            $scId = '102330';

            // if is regular payment
            if ($this->requestFields['regularPayment']) {
                /** @var  $shopId */
                $shopId = '138203';
                $scId = '102329';
            }

            // getting yandex link
            $link = 'https://money.yandex.ru/eshop.xml?shopId=' . $shopId . '&scid=' . $scId . '&paymentType=AC&sum=' . $price_rur .
                '&orderNumber=' . $this->requestFields['orderId'] .
                '&customerNumber=' . $this->requestFields['email'] .
                '&cps_email=' . $this->requestFields['email'];

            // if is regular payment
            if ($this->requestFields['regularPayment']) {
                // pushing token
                $link .= '&rebillingOn=true';
            }

            // getting redirected
            header('Location: ' . $link);
        } else {
            // if is regular payment
            if ($this->requestFields['regularPayment']) {
                $params = [
                    'email' => $this->requestFields['email'],
                    'productId' => $this->requestFields['productId'],
                    'productPrice' => $this->requestFields['productPrice'],
                    'productName' => $this->requestFields['productName'],
                    'regularMode' => $this->requestFields['regularMode'],
                ];

                // initialize
                $link = $this->regularPaymentHandler . '?' . http_build_query($params);
                // handle
                header('Location: ' . $link);
            } else {
                // if is Ukraine
                $this->actionGenerateInvoiceRequest();
            }
        }
    }

    /**
     * Getting request method
     *
     * @return string
     */
    protected function getMethod()
    {
        return 'api';
    }
}
