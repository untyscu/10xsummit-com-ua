<?php

/**
 * Created by PhpStorm.
 * User: Oleksandr Serdiuk
 * Date: 22.05.2017
 * Time: 10:47
 */
abstract class AbstractRequestController
{
    /**
     * Merchant Account in WayForPay
     */
    const MERCHANT_ACCOUNT = 'goldcoach_ru';

    /**
     * Merchant secret key for request
     */
    const MERCHANT_SIGNATURE = '4a4e36c1f8286dd00e39fe598f77c7ae2b721865';

    /**
     * Primary Api version. Default: 1
     */
    const API_VERSION = '1';

    /**
     * Page language ua | ru | en
     */
    const LANGUAGE = 'ru';

    /**
     * Data request back url for payment answer
     */
    const SERVICE_URL = 'https://goldcoach.ru/shop/wfp.php';

    /**
     * Api method for getting Request Url
     */
    const TRANSACTION_TYPE = 'CREATE_INVOICE';

    /**
     * Basic method for Auth in payment system
     */
    const MERCHANT_AUTH_TYPE = 'SimpleSignature';

    /**
     * Site domain name
     */
    const MERCHANT_DOMAIN_NAME = 'https://goldcoach.ru';

    /**
     * Timing for getting answer from payment system
     * Default 24 hours, currently 1h
     */
    const ORDER_TIMEOUT = '3600';

    /**
     * Payment system request url
     */
    const INVOICE_REQUEST_HOST = 'https://api.wayforpay.com/';

    /**
     * All methods for payment
     * Default All
     * Available: card | privat24 | lpTerminal
     */
    const PAYMENT_SYSTEMS = 'card;privat24;lpTerminal';

    /**
     * Payment currency
     */
    const PAYMENT_CURRENCY = 'USD';

    /**
     * Hash algorithm for the secret key
     */
    const HASH_ALGORITHM = 'md5';

    /**
     * Hash secret phrase
     */
    const HASH_SECRET_PHRASE = 'Invoice request for wayforpay';

    /**
     * Ok result for receiving answer
     * From payment system
     */
    const INVOICE_REQUEST_OK = 'Ok';

    /**
     * Invoice request main array
     * @var array
     */
    private $request = array();

    /**
     * Setting up request fields
     *
     * @var array
     */
    protected $requestFields;

    /**
     * Fields for generation of signature
     * Exists two types:
     * 1) Static: only strings
     * 2) Recursive: all array items
     * @var array
     */
    private $signatureFields = array(
        'static' => array(
            'merchantAccount', 'merchantDomainName',
            'orderReference', 'orderDate', 'amount',
            'currency',
        ),
        'recursive' => array(
            'order_products_names', 'order_products_count',
            'order_products_prices',
        ),
    );

    /**
     * Signature delimiter for fields
     * USED only for generation
     * @var string
     */
    private $signatureDelimiter = ';';

    /**
     * Signature encoding for fields
     * USED only for generation
     * @var string
     */
    private $signatureEncoding = 'UTF-8';

    /**
     * Includes basic constants in Request
     * For payment system
     * @var array
     */
    protected $invoiceRequestArray;

    /**
     * Pushing into invoice Request
     * basic params
     * InvoiceRequestListenerController constructor.
     */
    public function __construct()
    {
        $this->invoiceRequestArray = array(
            'transactionType' => self::TRANSACTION_TYPE,
            'merchantAccount' => self::MERCHANT_ACCOUNT,
            'merchantAuthType' => self::MERCHANT_AUTH_TYPE,
            'merchantDomainName' => self::MERCHANT_DOMAIN_NAME,
            'apiVersion' => self::API_VERSION,
            'serviceUrl' => self::SERVICE_URL,
            //'currency' => self::PAYMENT_CURRENCY,
            'paymentSystems' => self::PAYMENT_SYSTEMS,
        );
    }

    /**
     * Generating hash signature for payment system
     * Based by order attributes
     * @param array $orderRequest
     * @param array $requestArray
     * @return mixed
     */
    protected function actionGenerateMerchantSignature(array $orderRequest, array &$requestArray)
    {
        // generating empty string
        $signatureString = '';

        // conjunct static fields
        foreach ($this->signatureFields['static'] as $staticField)
        {
            $signatureString .= mb_convert_encoding($requestArray[$staticField], $this->signatureEncoding);
            $signatureString .= $this->signatureDelimiter;
        }

        foreach ($this->signatureFields['recursive'] as $recursiveField)
        {
            $needleArray = $orderRequest[$recursiveField];

            foreach ($needleArray as $field)
            {
                //$signatureString .= mb_convert_encoding($field, $this->signatureEncoding);
                $signatureString .= $field;
                $signatureString .= $this->signatureDelimiter;
            }
        }

        $signatureString = rtrim($signatureString, $this->signatureDelimiter);

        $requestArray['merchantSignature'] = hash_hmac(self::HASH_ALGORITHM, $signatureString, self::MERCHANT_SIGNATURE);

        return true;
    }

    /**
     * Primary method for collecting data
     * To send for payment system
     * In order to get payment Url
     *
     * @return mixed
     */
    abstract public function actionGenerateInvoiceRequest();

    /**
     * Pushing custom fields for
     * Order Request Array
     *
     * @param array $orderRequest
     * @return void
     */
    public function actionPushCustomFieldsForGenerationOfSignature(array &$orderRequest)
    {
        $orderRequest['merchantAccount'] = self::MERCHANT_ACCOUNT;
        $orderRequest['merchantDomainName'] = self::MERCHANT_DOMAIN_NAME;
    }

    /**
     * Merging payment data with basic
     * constants
     * @param $requestArray
     * @return array
     */
    protected function actionMergePaymentSystemArray(&$requestArray)
    {
        $mergedRequest = array_merge($requestArray, $this->invoiceRequestArray);

        return $mergedRequest;
    }

    /**
     * Setting Request array in JSON format
     * for payment system
     *
     * @param array $request
     */
    protected function setRequest(array $request)
    {
        $this->request = json_encode( $request );
    }

    /**
     * Getting Request array in JSON format
     * for payment system
     *
     * @return array
     */
    protected function getRequest()
    {
        return $this->request;
    }

    /**
     * Method for getting curl request for payment system
     * In order to get invoice Url for payment
     *
     * @return void
     */
    protected function actionMakeInvoiceRequest()
    {
        // getting stream
        $ch = curl_init();

        // setting request url
        curl_setopt ($ch, CURLOPT_URL, self::INVOICE_REQUEST_HOST . $this->getMethod());

        // Http POST method
        curl_setopt ($ch, CURLOPT_POST, 1);

        // Getting Invoice Request
        curl_setopt ($ch, CURLOPT_POSTFIELDS, $this->getRequest());

        // Return from server is required
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);

        // sending request to the server
        $serverOutput = curl_exec ($ch);

        // closing the stream
        curl_close ($ch);

        // setting Request result from JSON to Array
        $invoiceRequestResult = json_decode ($serverOutput);

        // if request is accepted
        if ($invoiceRequestResult->reason == self::INVOICE_REQUEST_OK)
        {
            // return redirect for payment page
            header('Location: ' . $invoiceRequestResult->invoiceUrl);
            die();
        }

        // else return 404
        header('Location: /');
        die();
    }

    final public function generateCollection()
    {
        $now = strtotime('now');
        $quantity = 1;

        $signature = self::MERCHANT_ACCOUNT . ';' . self::MERCHANT_DOMAIN_NAME . ';' . $this->requestFields['orderId'] .
            ';' . $now . ';' . $this->requestFields['productPrice'] .
            ';' . self::PAYMENT_CURRENCY . ';' . $this->requestFields['productName'] .
            ';' . $quantity . ';' . $this->requestFields['productPrice'];

        return [
            'orderReference' => $this->requestFields['orderId'],
            'orderDate' => $now,
            'merchantSignature' => hash_hmac(self::HASH_ALGORITHM, $signature, self::MERCHANT_SIGNATURE),

            'merchantAccount' => self::MERCHANT_ACCOUNT,
            'merchantDomainName' => self::MERCHANT_DOMAIN_NAME,
            'serviceUrl' => self::SERVICE_URL,
        ];
    }

    /**
     * Getting request method
     *
     * @return mixed
     */
    abstract protected function getMethod();
}
