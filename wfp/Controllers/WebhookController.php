<?php

// setting up server path
$server = $_SERVER['DOCUMENT_ROOT'];

// include storage
include_once $server . '/wfp/Storage/Storage.php';

/**
 * Created by PhpStorm.
 * User: Oleksandr Serdiuk
 * Date: 22.05.2017
 * Time: 10:23
 */
class WebhookController
{
    /**
     * Getting database instance
     *
     * @var Storage
     */
    private $db;

    /**
     * Ok status for payment system
     */
    const OK_TRANSACTION_STATUS = 'Approved';


    /**
     * Hash algorithm for the secret key
     */
    const HASH_ALGORITHM = 'md5';


    /**
     * Merchant secret key for request
     */
    const MERCHANT_SIGNATURE = '4a4e36c1f8286dd00e39fe598f77c7ae2b721865';

    /**
     * Signature delimiter for fields
     * USED only for generation
     * @var string
     */
    private $signatureDelimiter = ';';


    /**
     * Getting log file path
     */
    const LOG_FILE_PATH = '../error_log.txt';

    /**
     * Getting payment system answer
     * Throws exceptions
     * Logging warnings as well as exceptions
     *
     * @return string
     */
    public function actionParseRequest()
    {
        // trying
        try {
            // getting object
            $requestObject = json_decode(file_get_contents('php://input'), true);

            // if valid object found
            if (isset ($requestObject['transactionStatus'])) {
                // logging received data
                $this->actionLogPaymentWarningMessage('Status: ' . $requestObject['transactionStatus']);

                // if payment status is successful
                if ($requestObject['transactionStatus'] == self::OK_TRANSACTION_STATUS) {
                    // trying
                    try {
                        // save data
                        $this->save($requestObject);

                        // getting success answer
                        header('Content-Type: application/json');
                        echo json_encode(  $this->actionGeneratePaymentAnswerSignature($requestObject) );
                        die();

                        // handle
                    } catch (\Exception $e) {
                        // if any exceptions found - log
                        $this->actionLogPaymentExceptionMessage('WayForPay error', $e);
                    }
                } else {
                    // if payment status is not valid
                    header('Content-Type: application/json');
                    echo json_encode(  $this->actionGeneratePaymentAnswerSignature($requestObject, $requestObject['transactionStatus']) );


                    $this->actionLogPaymentWarningMessage(
                        'Incorrect payment status: ' . $requestObject['transactionStatus']
                    );
                    die();
                }
            } else {
                // if payment status is not valid
                $this->actionLogPaymentWarningMessage(
                    'Incorrect payment receive'
                );
            }




        } catch (\Exception $e) {
            $this->actionLogPaymentExceptionMessage('Entrance error: ', $e);
        }



        return 'success';
    }

    public function actionGeneratePaymentAnswerSignature(array $requestObject, $status = "accept")
    {

        $signatureString = '';
        $time = time();

        $signatureString .= $requestObject['orderReference'] . $this->signatureDelimiter;

        $signatureString .= "accept" . $this->signatureDelimiter;
        $signatureString .= $time;

        $answerArray = array();

        $answerArray['orderReference'] = $requestObject['orderReference'];
        $answerArray['status'] = $status;
        $answerArray['time'] = $time;

        $answerArray['signature'] = hash_hmac(self::HASH_ALGORITHM, $signatureString, self::MERCHANT_SIGNATURE);

        return $answerArray;

    }

    /**
     * Logging an exception message
     * @param $header
     * @param \Exception $e
     */
    private function actionLogPaymentExceptionMessage($header, \Exception $e)
    {
        // Открываем файл для получения существующего содержимого
        $current = file_get_contents(self::LOG_FILE_PATH);

        // Добавляем нового человека в файл
        $current .= 'Header: ' . $header . ', File: ' .
            $e->getFile() . ', Line: ' . $e->getLine() .
            ', Message: ' . $e->getMessage() . "\n";

        // Пишем содержимое обратно в файл
        file_put_contents(self::LOG_FILE_PATH, $current);
    }

    /**
     * Logging an warning message
     * @param $header
     */
    private function actionLogPaymentWarningMessage($header)
    {
        // Открываем файл для получения существующего содержимого
        $current = file_get_contents(self::LOG_FILE_PATH);

        // Добавляем нового человека в файл
        $current .= 'Warning: ' . $header . "\n";

        // Пишем содержимое обратно в файл
        file_put_contents(self::LOG_FILE_PATH, $current);
    }
}