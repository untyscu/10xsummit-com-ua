<?php

/**
 * Created by PhpStorm.
 * User: oleksandrserdiuk
 * Date: 07.07.16
 * Time: 9:27 AM
 */

/**
 * Connection with Database
 * via @PDO
 * @save connection
 * Class Storage
 */
class Storage
{
    private $_db;
    static $_instance;

    public $query;

    /**
     * Make an instance of @PDO
     * Set Database @charset to UTF-8
     * Storage constructor.
     */
    private function __construct()
    {
        $this->_db = new PDO('mysql:host=localhost;dbname=billing', 'billing', 'AQfir0RF');
        $this->_db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $this->setCharset();
    }

    /**
     * Disable clone connection via @PDO
     */
    private function __clone(){}

    /**
     * Singleton Design Pattern
     * for @PDO Instance
     * @return Storage
     */
    public static function getInstance() {

        if (!(self::$_instance instanceof self)) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    /**
     * Make basic query for @PDO
     * @param $sql
     * @return PDOStatement
     */
    public function query( $sql ) {

        return $this->_db->query($this->_db, $sql);
    }

    /**
     * Prepare PDOStatement for @PDO
     * Connection with Database
     * @param $query
     * @return PDOStatement
     */
    public function prepare( $query ) {

        $this->query = $query;

        return $this->_db->prepare( $this->query );
    }

    /**
     * Execute an PDOStatement Query
     * for @PDO connection with Database
     * @param $query
     * @param $array
     * @return bool
     */
    public function execute( $query, $array ) {

        $prepared_stmt = $this->prepare( $query );

        foreach ( $array as $key => $value ) {

            $prepared_stmt
                ->bindValue( $key, $value);
        }

        return $prepared_stmt->execute();
    }

    /**
     * Make An Select Request To Database
     * Via @PDO Connection
     * @param $query
     * @param $array
     * @return PDOStatement
     */
    public function select( $query, $array ) {

        $this->query = $query;

        $prepared_stmt = $this->_db->prepare( $this->query );

        if($array != NULL) {
            foreach ($array as $key => $value) {

                $prepared_stmt
                    ->bindValue($key, $value);
            }
        }

        return $prepared_stmt;
    }

    /**
     * Set Database via @PDO connection
     * Charset To UTF-8
     */
    public function setCharset() {

        $this->_db->prepare("SET NAMES utf8")->execute();
    }
}