<?php

// getting server root
$server = $_SERVER['DOCUMENT_ROOT'];

// setting up test data
$email = $_GET['email'];
$productId = $_GET['productId'];
$productPrice = $_GET['productPrice'];
$productName = $_GET['productName'];

// include request handler
include_once $server . '/wfp/Controllers/RequestController.php';

// setting up controller
$controller = new RequestController($productId, $productPrice, $productName, $email, false, isset($_GET['currency'])?$_GET['currency']:'USD');
$controller->actionGenerateInvoiceRequest();
