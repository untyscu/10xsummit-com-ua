<?php header('Access-Control-Allow-Origin: *'); ?>
<?php
//echo $_SERVER['HTTP_HOST'];
$get = '';
if(isset($_SERVER['REQUEST_URI']) && $_SERVER['REQUEST_URI'] != ''){
    $get = $_SERVER['REQUEST_URI'];
}
$z = array_reverse(explode('.', $_SERVER['HTTP_HOST']));
$country = geoip_country_code_by_name($_SERVER['REMOTE_ADDR']);

if($z[0] == 'ua' && $country == 'RU'){
	#echo '1';
	header('Location: http://10xsummit.ru'.$get);
}

if($z[0] == 'ru' && $country != 'RU'){
	#echo '2';
	header('Location: http://10xsummit.com.ua'.$get);
}

$silver_price = 57;
$gold_price = 77;
$gold_pluse_price = 117;
$vip_price = 397; 

if(strtotime(date("d-m-Y "))>strtotime("04-10-2018 ") || $_GET['mode'] == 'test'){
    $silver_price = 67;
    $gold_price = 87;
    $gold_pluse_price = 127;
    $vip_price = 437;  
}

?>
<!DOCTYPE html>
<html lang="ru">
	<head>
		<!-- Google Analytics Content Experiment code -->
		<script>function utmx_section(){}function utmx(){}(function(){var
		k='183316954-296',d=document,l=d.location,c=d.cookie;
		if(l.search.indexOf('utm_expid='+k)>0)return;
		function f(n){if(c){var i=c.indexOf(n+'=');if(i>-1){var j=c.
		indexOf(';',i);return escape(c.substring(i+n.length+1,j<0?c.
		length:j))}}}var x=f('__utmx'),xx=f('__utmxx'),h=l.hash;d.write(
		'<sc'+'ript src="'+'http'+(l.protocol=='https:'?'s://ssl':
		'://www')+'.google-analytics.com/ga_exp.js?'+'utmxkey='+k+
		'&utmx='+(x?x:'')+'&utmxx='+(xx?xx:'')+'&utmxtime='+new Date().
		valueOf()+(h?'&utmxhash='+escape(h.substr(1)):'')+
		'" type="text/javascript" charset="utf-8"></sc'+'ript>')})();
		</script><script>utmx('url','A/B');</script>
		<!-- End of Google Analytics Content Experiment code -->
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<title>10XСАММИТ - Бизнес-Событие №1 в РФ и СНГ по контенту</title>
		<!--Optimize-->
		<style>.async-hide { opacity: 0 !important} </style>
		<script>(function(a,s,y,n,c,h,i,d,e){s.className+=' '+y;h.start=1*new Date;
		h.end=i=function(){s.className=s.className.replace(RegExp(' ?'+y),'')};
		(a[n]=a[n]||[]).hide=h;setTimeout(function(){i();h.end=null},c);h.timeout=c;
		})(window,document.documentElement,'async-hide','dataLayer',4000,
		{'GTM-WJ3GRB5':true});</script>
		<!--Analytic-->
		<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
		ga('create', 'UA-38087671-1', 'auto');
		ga('require', 'GTM-WJ3GRB5');
		ga('send', 'pageview');
		</script>
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-38087671-2"></script>
		<script>
			window.dataLayer = window.dataLayer || [];
			function gtag(){dataLayer.push(arguments);}
			gtag('js', new Date());

			gtag('config', 'UA-38087671-2');
		</script>
		<!-- Google Tag Manager -->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-PDFPNHM');</script>
		<!-- End Google Tag Manager -->
        <meta name="description" content="Современные бизнес-технологии 10X от прогрессивных предпринимателей и топовых экспертов для кратного роста в 2019 году!">
		<meta name="keywords" content="" />
		<link rel="stylesheet" href="src/css/style.min.css?v1.5" type="text/css"  media="all" />

        <!--for Facebook-->
        <meta property="og:url"           content="http://10xsummit.com.ua/" />
        <meta property="og:type"          content="website" />
        <meta property="og:title"         content="Бизнес-конференция No1 в Украине, РФ и СНГ по контенту" />
        <meta property="og:description"   content="Современные бизнес-технологии 10X от прогрессивных предпринимателей и топовых экспертов для кратного роста в 2019 году!" />
        <meta property="og:image"         content="./src/img/opengraph/fb.png" />
        <meta property="og:image:width"   content="1200" />
        <meta property="og:image:height"  content="628" />
        <!--/for Facebook-->

        


		<link rel="apple-touch-icon" sizes="57x57" href="favicon/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="favicon/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="favicon/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="favicon/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="favicon/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="favicon/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="favicon/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="favicon/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="192x192"  href="favicon/android-icon-192x192.png">
		<link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="96x96" href="favicon/favicon-96x96.png">
		<link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png">
		<link rel="manifest" href="favicon/manifest.json">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="favicon/ms-icon-144x144.png">
		<meta name="theme-color" content="#ffffff">
		<script type="text/javascript" src="/js/share.js?93" charset="windows-1251"></script>
	</head>
	<body>
		<!-- Google Tag Manager (noscript) -->
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PDFPNHM"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->
		<!-- Facebook Pixel Code -->
    <script>
      !function(f,b,e,v,n,t,s)
      {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
      n.callMethod.apply(n,arguments):n.queue.push(arguments)};
      if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
      n.queue=[];t=b.createElement(e);t.async=!0;
      t.src=v;s=b.getElementsByTagName(e)[0];
      s.parentNode.insertBefore(t,s)}(window, document,'script',
      'https://connect.facebook.net/en_US/fbevents.js');
      fbq('init', '1619239984767327');
      fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
      src="https://www.facebook.com/tr?id=1619239984767327&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Facebook Pixel Code --> 

		<style>
			@media all and (max-width: 600px) {
					#timercd00c4f5c6bb427a1acbd814145349b0 .timer-flipchart-face,
					.timer-flipchart-card,
					#timercd00c4f5c6bb427a1acbd814145349b00 .timer-flipchart-face{
						font-size: 28px;
					
					}
					.timer-separator{
						padding: 0 3px !important;
					}
					#timercd00c4f5c6bb427a1acbd814145349b0,
					#timercd00c4f5c6bb427a1acbd814145349b00{
						    min-width: 310px !important;
					}
				}
		</style>
		<!--fb share-
		<div id="fb-root">
		</div>
		<script>(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = 'https://connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v3.1';
		fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>
		!--/fb share-->
		<nav class="topnav" id="topNav">
			<div class="container row">
				<a href="#top" class="topnav__logo">10x&nbsp;Саммит&nbsp;2018</a>
				<div class="topnav__menu-cover"></div>
				<div class="topnav__time tooltip">
					<span class="tooltip__icon">
						<i class="icon-clock"></i>
					</span>
					<span class="tooltip__text-bottom">
						На связи: Пн. - Пт.,  9:00 - 18:00 [МСК]
					</span>
				</div>
				<div class="topnav__phones">
					<p class="phones-ru">+7  (495) 133-82-24</p>
					<p class="phones-ua">+38  (044) 221-32-24</p>
				</div>
			</div>
		</nav>
		<section class="thanks-page">
			<div class="thanks-header">
				<span class="header__crystal-1"></span>
				<span class="header__crystal-2"></span>
				<span class="header__crystal-3"></span>
				<span class="header__crystal-4"></span>
				<h1 class="thanks-header__title scrollreveal">
				<span class=thanks-header__title-big>ПОЗДРАВЛЯЕМ</span><br>
				<span class="thanks-header__title__date">ВЫ ТОЛЬКО ЧТО СОВЕРШИЛИ <span class="desktop-none"><br/></span><strong>10X</strong>&nbsp;ДЕЙСТВИЕ!</span>
				</h1>
				 <p class="thanks-header__intro">Мы предварительно заняли вам место. В ближайшее время мы&nbsp;свяжемся с вами для завершения бронирования</p>
          		
          		
           		<p style="color: red; font-weight: bold;font-size: 20px;">СЛЕДУЮЩЕЕ ПОВЫШЕНИЕ ЦЕНЫ ЧЕРЕЗ</p>
            </div>
         
            <script src="megatimer-thanks.js?ver=1.01"></script>
            <div class="container">
                <div class="thanks__block price__block"><span class="text">

                    <h2 class="price__title">Выберите свой вариант участия</h2>
                    <div class="price__intro">Оплатите сейчас с дополнительной 5% скидкой</div>
						<div class="pricelist">
							<div class="pricelist__item scrollreveal">
								<h5 class="pricelist__item__title silver">
								<i class="icon-price"></i>
								<span>SILVER</span>
								</h5>
								<!-- <p class="pricelist__price-striked"><span>3,799 грн</span></p>
								<p class="pricelist__price-big">1,099 грн</p>
								<p class="pricelist__price-small">2,799 руб</p>
								<p class="pricelist__price-small">47 $</p> -->
								<p class="pricelist__price-striked"><span>167 $</span></p>
								<p class="pricelist__price-big"><?=$silver_price;?> $</p>
								<hr>
								<p>Участие в мероприятии</p>
								<hr>
								<p>Пакет участника</p>
								<hr>
								<p>Доступ к стенду <b>NETWORKING</b></p>
								<hr>
								<p>Участие в розыгрыше призов от партнеров</p>
								<hr>
								<p>Место в зале в секторе <b>«SILVER»</b></p>
								<hr>
								<a href="" class="btn-orange2 payment-link" data-mopsbbk="8CB0D3C682B6848C1C8C6C52:335422BBE7227F4B9BE44FA8" data-uid="p2c9448f768">купить билет</a>
							</div>
							<div class="pricelist__item scrollreveal">
								<h5 class="pricelist__item__title gold">
								<i class="icon-price"></i>
								<span>GOLD</span>
								</h5>
								<!-- <p class="pricelist__price-striked"><span>5,099 грн</span></p>
								<p class="pricelist__price-big">1,699 грн</p>
								<p class="pricelist__price-small">4,199 руб</p>
								<p class="pricelist__price-small">67 $</p> -->
								<p class="pricelist__price-striked"><span>197 $</span></p>
								<p class="pricelist__price-big"><?=$gold_price;?> $</p>
								<hr>
								<p>Возможности тарифа <b>SILVER</b></p>
								<hr>
								<p>Место в зале в секторе <b>«GOLD»</b></p>
								<hr>
								<p>Видео-запись форума</p>
								<hr>
								<p>Презентации спикеров</p>
								<hr>
								<p>3-недельная поддержка экспертов системы бизнес-роста</p>
								<hr>
								<a href="" class="btn-orange2 payment-link" data-mopsbbk="617ED3B5A887895493C1765D:FFDD7C4EC4A053D5ED0145CE" data-uid="p2c9448f769">купить билет</a>
							</div>
							<div class="pricelist__item scrollreveal">
								<h5 class="pricelist__item__title gold-plus">
								<i class="icon-price"></i>
								<span>GOLD+</span>
								</h5>
								<!-- <p class="pricelist__price-striked"><span>6,499 грн</span></p>
								<p class="pricelist__price-big">2,599 грн</p>
								<p class="pricelist__price-small">5,899 руб</p>
								<p class="pricelist__price-small">97 $</p> -->
								<p class="pricelist__price-striked"><span>237 $</span></p>
								<p class="pricelist__price-big"><?=$gold_pluse_price;?> $</p>
								<hr>
								<p>Возможности тарифа <b>GOLD</b></p>
								<hr>
								<p>Место в зале в секторе <b>«GOLD+»</b></p>
								<hr>
								<p>Онлайн-программа от <b>GOLDCOACH</b> (на выбор)</p>
								<hr>
								<p>Отдельная стойка регистрации</p>
								<hr>
								<p>Отдельный вход в зал (без очереди)</p>
								<hr>
								<a href="" class="btn-orange2 payment-link" data-mopsbbk="1FB852AD87FB989D0E9E9F38:9554AE09B77A863C712E9770" data-uid="p2c9448f770">купить билет</a>
							</div>
							<div class="pricelist__item scrollreveal">
								<h5 class="pricelist__item__title vip">
								<i class="icon-price"></i>
								<span>VIP</span>
								</h5>
								<!-- <p class="pricelist__price-striked"><span>17,999 грн</span></p>
								<p class="pricelist__price-big">9,199 грн</p>
								<p class="pricelist__price-small">21,099 руб</p>
								<p class="pricelist__price-small">357 $</p> -->
								<p class="pricelist__price-striked"><span>647 $</span></p>
								<p class="pricelist__price-big"><?=$vip_price;?> $</p>
								<hr>
								<p>Возможности тарифа <b>GOLD+</b></p>
								<hr>
								<p>Место в зале в секторе <b>«VIP»</b></p>
								<hr>
								<p>Именной VIP- бейдж</p>
								<hr>
								<p>Участие в круглом столе с экспертами GC и спикерами</p>
								<hr>
								<p>Доступ к бизнес-фуршету во время обеденного перерыва</p>
								<hr>
								<p>Закрытая групповая VIP - встреча во время бизнес фуршета</p>
								<hr>
								<p>Закрытая фото-сессия</p>
								<hr>
								<p>Приглашение на закрытый VIP-ужин</p>
								<hr>
								<a href="" class="btn-orange2 payment-link" data-mopsbbk="1FF96CCA42F7A113462EBDD2:5E721F84A0189043BF03CBE5" data-uid="p2c9448f771">купить билет</a>
							</div>
						</div>
					</span>
				</div>
				<!--/pricelist-->
				<div class="price__timeline">
                    <p class="timeline_note">Цена будет расти на 15 %<br /> каждые 2-3 недели</p>
                    <ul class="timeline">
                        <li class="timeline__line timeline__line-up timeline__line-1" data-time-start="2018-08-20" data-time-end="2018-09-13">
                            <div class="timeline__line-red" style="width: 100%"></div>

                        </li>
                        <li class="timeline__dot timeline__dot-big dot-1" data-time-start="2018-09-13" data-time-end="2018-09-14">
                            <div class="timeline__dot-big__rounds"></div>

                            <p class="timeline__dot-big__price">+15%</p>
                            <p class="timeline__dot-big__date">20 сентября</p>
                        </li>
                        <li class="timeline__line timeline__line-down timeline__line-2" data-time-start="2018-09-14" data-time-end="2018-09-24">
                            <div class="timeline__line-red"></div>

                        </li>
                        <li class="timeline__dot timeline__dot-small dot-2" data-time-start="2018-09-24" data-time-end="2018-09-25"></li>
                        <li class="timeline__line timeline__line-up timeline__line-3" data-time-start="2018-09-25" data-time-end="2018-10-04">
                            <div class="timeline__line-red"></div>

                        </li>
                        <li class="timeline__dot timeline__dot-big dot-3" data-time-start="2018-10-04" data-time-end="2018-10-05">
                            <div class="timeline__dot-big__rounds"></div>

                            <p class="timeline__dot-big__price">+15%</p>
                            <p class="timeline__dot-big__date">4 октября</p>
                        </li>
                        <li class="timeline__line timeline__line-down timeline__line-4" data-time-start="2018-10-05" data-time-end="2018-10-15">
                            <div class="timeline__line-red"></div>

                        </li>
                        <li class="timeline__dot timeline__dot-small dot-4" data-time-start="2018-10-15" data-time-end="2018-10-16"></li>
                        <li class="timeline__line timeline__line-up timeline__line-5" data-time-start="2018-10-16" data-time-end="2018-10-25">
                            <div class="timeline__line-red"></div>

                        </li>
                        <li class="timeline__dot timeline__dot-big dot-5" data-time-start="2018-10-25" data-time-end="2018-10-26">
                            <div class="timeline__dot-big__rounds"></div>

                            <p class="timeline__dot-big__price">+15%</p>
                            <p class="timeline__dot-big__date">25 октября</p>
                        </li>
                        <li class="timeline__line timeline__line-down timeline__line-6" data-time-start="2018-10-26" data-time-end="2018-11-02">
                            <div class="timeline__line-red"></div>

                        </li>
                        <li class="timeline__dot timeline__dot-small dot-6" data-time-start="2018-11-02" data-time-end="2018-11-03"></li>
                        <li class="timeline__line timeline__line-up timeline__line-7" data-time-start="2018-11-03" data-time-end="2018-11-10">
                            <div class="timeline__line-red"></div>

                        </li>
                        <li class="timeline__dot timeline__dot-big dot-7" data-time-start="2018-11-10" data-time-end="2018-11-11">
                            <div class="timeline__dot-big__rounds"></div>

                            <p class="timeline__dot-big__price">День ивента</p>
                            <p class="timeline__dot-big__date">10 ноября</p>
                        </li>
                        <!-- <li class="timeline__line timeline__line-down timeline__line-8" data-time-start="2018-10-30" data-time-end="2018-11-10">
                                    <div class="timeline__line-red"></div>

                        </li> -->
                    </ul>
                    
                </div>
                
			</div>
		
			<div style="padding-top: 100px;">
				<script src="megatimer-thanks.js?ver=1.01"></script>
			</div>
	
			<div class="corp-offer">
                <div class="container">
                    <div class="corp-offer__offer">
                        <div class="text">
                            <h4 class="corp-offer__title">Приводите друзей - вместе выгоднее!</h4>
                            <p class="corp-offer__intro">Получите до 25% скидки на всех, если идёте с друзьями. Чем больше друзей - тем ниже цена билетов для всех!</p>
                            <a href="#regform_group" class="open-modal btn-orange3">Забрать скидку</a>
                        </div>
                        <div class="crystal-1"></div>

                        <div class="crystal-2"></div>

                        <div class="crystal-3"></div>

                        <div class="crystal-4"></div>

                        <div class="crystal-5"></div>

                        <div class="crystal-6"></div>

                    </div>
                    <div class="thanks__block guarantee__block">
                        <span class="text">

                            <h2>Двойная гарантия возврата денег</h2>
                            <img class="guarantee__img" src="src/img/thanks/medal.png" alt="">
                            <p class="corp-offer__garantee__text scrollreveal">1. Если по какой-то причине не сможете приехать на саммит, вернём деньги за билет. Для этого просто напишите в любой из наших чатов или на <a href="mailto:support@goldcoach.ru">support@goldcoach.ru</a>.</p>
                            <p class="corp-offer__garantee__text scrollreveal">2. Кроме того, гарантия действует на саммите. Если вы решите, что это событие не стоит потраченных денег, подойдите к столику регистрации. Вы получите полный возврат за билет. Гарантия действует до обеда первого дня.</p>
                        </span>
                    </div>
                </div>
            </div>
		</section>
		<section class="prefooter">
			<div class="container row">
				<div class="prefooter_call">
					<p><b>Ответим</b> на ваши вопросы:</p>
					<p>
						<a href="mailto:support@goldcoach.ru">
							<i class="icon-f_env"></i>
							<span>support@goldcoach.ru</span>
						</a>
					</p>
					<div>
						<a href="http://m.me/goldcoach.ru/" target="_blank">
							<i class="icon-chat_fb"></i>
						</a>
						<a href="https://vk.me/-168188452" target="_blank">
							<i class="icon-chat_vk"></i>
						</a>
						<a href="viber://pa?chatURI=goldcoach" target="_blank">
							<i class="icon-chat_w"></i>
						</a>
						<a href="https://t.me/goldcoachbot" target="_blank">
							<i class="icon-chat_telega"></i>
						</a>
					</div>
					<div>
						<div class="tooltip prefooter__tooltip">
							<span class="tooltip__icon btn-orange2">
								Заказать звонок
							</span>
							<span class="tooltip__text-bottom">
								
								<form class="form-noreload"  action="https://forms.ontraport.com/v2.4/form_processor.php?" method="post" accept-charset="UTF-8">
                                
								    Введите свой телефон - мы перевзоним вам в ближайшее время.
									<input name="utmspecial_428" type="hidden" value="<?=isset($_GET['utm_special'])?$_GET['utm_special']:''?>"/>
									<input name="utmcampaig_224" type="hidden" value="<?=isset($_GET['utm_campaign'])?$_GET['utm_campaign']:''?>"/>
									<input name="utmmedium_225" type="hidden" value="<?=isset($_GET['utm_medium']) ? $_GET['utm_medium']:''?>"/>
									<input name="utmsource_223" type="hidden" value="<?=isset($_GET['utm_source']) ? $_GET['utm_source']:''?>"/>
									<input name="afft_" type="hidden" value=""/>
									<input name="aff_" type="hidden" value=""/>
									<input name="sess_" type="hidden" value=""/>
									<input name="ref_" type="hidden" value=""/>
									<input name="own_" type="hidden" value=""/>
									<input name="oprid" type="hidden" value=""/>
									<input name="contact_id" type="hidden" value=""/>
									<input name="utm_source" type="hidden" value="<?=isset($_GET['utm_source']) ? $_GET['utm_source']:''?>"/>
									<input name="utm_medium" type="hidden" value="<?=isset($_GET['utm_medium']) ? $_GET['utm_medium']:''?>"/>
									<input name="utm_term" type="hidden" value="<?=isset($_GET['utm_term']) ? $_GET['utm_term']:''?>"/>
									<input name="utm_content" type="hidden" value="<?=isset($_GET['utm_content']) ? $_GET['utm_content']:''?>"/>
									<input name="utm_campaign" type="hidden" value="<?=isset($_GET['utm_campaign']) ? $_GET['utm_campaign']:''?>"/>
									<input name="referral_page" type="hidden" value=""/>
									<input name="uid" type="hidden" value="p2c9448f769"/>
									<input name="mopsbbk" type="hidden" value="617ED3B5A887895493C1765D:FFDD7C4EC4A053D5ED0145CE"/>
									<input name="cell_phone" required type="text" class="big-input" value="" placeholder="Ваш номер телефона"/>
									<div class="moonray-form-element-wrapper moonray-form-element-wrapper-alignment-left moonray-form-input-type-submit">
										<input type="submit" name="submit-button" value="отправить" class="btn-orange2" id="mr-field-element-584743536048"/>
									</div>
								</form>
								<span class="form-thanks">
                                    <b>Заявку приняли.<br>Ожидайте звонка в ближайшее время!</b>
                                    <br>
                                    <hr>
                                    Мы работаем: ПН-ПТ, 9:00 - 18:00 МСК
                                </span>
							</span>
						</div>
					</div>
				</div>
				<div class="prefooter_follow">
					<p>Подписывайтесь на каналы организатора и следите за новостями по саммиту</p>
					<div class="justify">
						<a href="https://www.facebook.com/zimbitsky" target="_blank" rel="nofollow">
							<i class="icon-fb_r"></i>
						</a>
						<a href="https://vk.com/ivanzimbitskiy" target="_blank" rel="nofollow">
							<i class="icon-vk_r"></i>
						</a>
						<a href="https://instagram.com/izimbitskiy/" target="_blank" rel="nofollow">
							<i class="icon-insta_r"></i>
						</a>
						<a href="https://www.youtube.com/user/izimbitskiy" target="_blank" rel="nofollow">
							<i class="icon-youtube_r"></i>
						</a>
						<a href="https://t.me/zimbitskiy" target="_blank" rel="nofollow">
							<i class="icon-telega_r"></i>
						</a>
					</div>
				</div>
				<div class="prefooter_share">
					<p>Таким событием стоит поделиться с друзьями:</p>
					<div class="center">
						<div id="vk_share">
							<a href="https://vk.com/share.php?url=http%3A%2F%2F10xsummit.ru&title=%D0%91%D0%B8%D0%B7%D0%BD%D0%B5%D1%81-%D0%BA%D0%BE%D0%BD%D1%84%D0%B5%D1%80%D0%B5%D0%BD%D1%86%D0%B8%D1%8F%20%E2%84%961%20%D0%B2%20%D0%A0%D0%A4%20%D0%B8%20%D0%A1%D0%9D%D0%93%20%D0%BF%D0%BE%20%D0%BA%D0%BE%D0%BD%D1%82%D0%B5%D0%BD%D1%82%D1%83&description=%D0%A1%D0%BE%D0%B2%D1%80%D0%B5%D0%BC%D0%B5%D0%BD%D0%BD%D1%8B%D0%B5%20%D0%B1%D0%B8%D0%B7%D0%BD%D0%B5%D1%81-%D1%82%D0%B5%D1%85%D0%BD%D0%BE%D0%BB%D0%BE%D0%B3%D0%B8%D0%B8%2010X%20%D0%BE%D1%82%20%D0%BF%D1%80%D0%BE%D0%B3%D1%80%D0%B5%D1%81%D1%81%D0%B8%D0%B2%D0%BD%D1%8B%D1%85%20%D0%BF%D1%80%D0%B5%D0%B4%D0%BF%D1%80%D0%B8%D0%BD%D0%B8%D0%BC%D0%B0%D1%82%D0%B5%D0%BB%D0%B5%D0%B9%20%D0%B8%20%D1%82%D0%BE%D0%BF%D0%BE%D0%B2%D1%8B%D1%85%20%D1%8D%D0%BA%D1%81%D0%BF%D0%B5%D1%80%D1%82%D0%BE%D0%B2%20%D0%B4%D0%BB%D1%8F%20%D0%BA%D1%80%D0%B0%D1%82%D0%BD%D0%BE%D0%B3%D0%BE%20%D1%80%D0%BE%D1%81%D1%82%D0%B0%20%D0%B2%202019%20%D0%B3%D0%BE%D0%B4%D1%83!&image=http%3A%2F%2F10xsummit.ru%2Fsrc%2Fimg%2Fopengraph%2Ffb.png" target="_blank"></a>
                    </div>

                    <!--https://developers.facebook.com/docs/plugins/share-button?locale=ru_RU-->
                    <div class="fb-share-button" data-href="http://10xsummit.ru/" data-layout="button" data-size="large" data-mobile-iframe="false"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2F10xsummit.ru" class="fb-xfbml-parse-ignore">
                        <i class="icon-fb"></i>
                    </a></div>
					</div>
				</div>
			</div>
		</section>
		<footer class="footer">
            <div class="container row">
                <div class="footer_col">
                    <p>
                        <a href="/" class="footer__logo">
                            <img src="src/img/footer/logo.png" alt="10x&nbsp;Саммит">
                        </a>
                    </p>
                    <p class="mobile-none"><a href="">Политика конфиденциальности</a></p>
                    <p class="mobile-none">© ColdCoach.ru 2011 - 2018</p>
                </div>
                <div class="footer_col">
                    <p><a href="#regform_partner" class="open-modal">Стать партнером саммита</a></p>
                    <p><a href="#regform_sponsor" class="open-modal">Стать спонсором</a></p>
                </div>
                <div class="footer_col">
                    <p>Разработка и продвижение</p>
                    <p><a class="footer__dev" href="https://5xprofit.ru/?utm_source=10xsummit" target="_blank"><img src="src/img/footer/create_logo.png" alt="" style="width: 175px; height: 40px;"></a></p>
                </div>
                <div class="footer_col footer_mobile-show">
                    <p><a href="">Политика конфиденциальности</a></p>
                    <p>© ColdCoach.ru 2011 - 2018</p>
                </div>
            </div>
        </footer>
		<!--modal forms-->
		<div class="modal__fon" id="modalFon">
		</div>
		<!--Partners-->
		<div class="modal modal__partners" id="regform_partner">
			<div class="text">
				<span class="close-modal">x</span>
				<div class="modal__title">
					<div class="modal__title-text">
						<strong>Присоединяйтесь</strong> к саммиту как партнер!
					</div>
				</div>
				<form class="form-noreload"  action="https://forms.ontraport.com/v2.4/form_processor.php?" method="post" accept-charset="UTF-8">
					<p>Если вы хотите стать информационным
					партнёром саммита, заполните форму: </p>
					<input name="firstname" type="text"  required value="" placeholder="Ваше имя" class="big-input"/>
					<input name="email" type="email" required value="" placeholder="Ваш e-mail" class="big-input"/>
					<input name="cell_phone" required type="text" value="" placeholder="Ваш мобильный телефон" class="big-input"/>
					<input name="company" type="text" placeholder="Компания" class="big-input"/>
					<input name="utmspecial_428" type="hidden" value="<?=isset($_GET['utm_special']) ? $_GET['utm_special']:''?>"/>
					<input name="utmcampaig_224" type="hidden" value="<?=isset($_GET['utm_campaign']) ? $_GET['utm_campaign']:''?>"/>
					<input name="utmmedium_225" type="hidden" value="<?=isset($_GET['utm_medium']) ? $_GET['utm_medium']:''?>"/>
					<input name="utmsource_223" type="hidden" value="<?=isset($_GET['utm_source']) ? $_GET['utm_source']:''?>"/>
					<div class="moonray-form-element-wrapper moonray-form-element-wrapper-alignment-left moonray-form-input-type-submit">
						<button type="submit" name="submit-button" value="узнать о рекламных возможностях" class="btn-orange3" id="mr-field-element-584743536048">Стать партнёром</button>
					</div>
					<input name="afft_" type="hidden" value=""/>
					<input name="aff_" type="hidden" value=""/>
					<input name="sess_" type="hidden" value=""/>
					<input name="ref_" type="hidden" value=""/>
					<input name="own_" type="hidden" value=""/>
					<input name="oprid" type="hidden" value=""/>
					<input name="contact_id" type="hidden" value=""/>
					<input name="utm_source" type="hidden" value="<?=isset($_GET['utm_source']) ? $_GET['utm_source']:''?>"/>
					<input name="utm_medium" type="hidden" value="<?=isset($_GET['utm_medium']) ? $_GET['utm_medium']:''?>"/>
					<input name="utm_term" type="hidden" value=""/>
					<input name="utm_content" type="hidden" value="<?=isset($_GET['utm_content']) ? $_GET['utm_content']:''?>"/>
					<input name="utm_campaign" type="hidden" value="<?=isset($_GET['utm_campaign']) ? $_GET['utm_campaign']:''?>"/>
					<input name="referral_page" type="hidden" value=""/>
					<input name="uid" type="hidden" value="p2c9448f772"/>
					<input name="mopsbbk" type="hidden" value="F254189B844FE80C5F46A499:951A4F0120031D4E3312B78D"/>
				</form>
				<div class="form-thanks">
                    <h2>Поздравляем с умным решением! </h2>
                    <p>Ожидайте звонка в течение дня в рабочее время.
                    </p>
                    <p>Мы работаем: ПН - ПТ, 9:00 - 18:00 (МСК)</p>
                </div>
			</div>
		</div>
		<!--Sponsors-->
		<div class="modal modal__partners" id="regform_sponsor">
			<div class="text">
				<span class="close-modal">x</span>
				<div class="modal__title">
					<div class="modal__title-text">
						<strong>Присоединяйтесь</strong> к саммиту как спонсор для усиления своего бренда и потока клиентов
					</div>
				</div>
				<form class="form-noreload"  action="https://forms.ontraport.com/v2.4/form_processor.php?" method="post" accept-charset="UTF-8">
					<p>10X САММИТ &mdash; это возможность получить поток самых качественных клиентов в кратчайшие сроки и укрепить свой бренд в головах наиболее активной аудитории предпринимателей, топ-менеджеров Украины и России, готовых инвестировать деньги в развитие себя и компании.</p>
					<input name="firstname" type="text"  required value="" placeholder="Ваше имя" class="big-input"/>
					<input name="email" type="email" required value="" placeholder="Ваш e-mail" class="big-input"/>
					<input name="cell_phone" required type="text" value="" placeholder="Ваш мобильный телефон" class="big-input"/>
					<input name="company" type="text" placeholder="Компания" class="big-input"/>
					<input name="utmspecial_428" type="hidden" value="<?=isset($_GET['utm_special']) ? $_GET['utm_special']:''?>"/>
					<input name="utmcampaig_224" type="hidden" value="<?=isset($_GET['utm_campaign']) ? $_GET['utm_campaign']:''?>"/>
					<input name="utmmedium_225" type="hidden" value="<?=isset($_GET['utm_medium']) ? $_GET['utm_medium']:''?>"/>
					<input name="utmsource_223" type="hidden" value="<?=isset($_GET['utm_source']) ? $_GET['utm_source']:''?>"/>
					<div class="moonray-form-element-wrapper moonray-form-element-wrapper-alignment-left moonray-form-input-type-submit">
						<button type="submit" name="submit-button" value="узнать о рекламных возможностях" class="btn-orange3" id="mr-field-element-584743536048">Стать спонсором</button>
					</div>
					<input name="afft_" type="hidden" value=""/>
					<input name="aff_" type="hidden" value=""/>
					<input name="sess_" type="hidden" value=""/>
					<input name="ref_" type="hidden" value=""/>
					<input name="own_" type="hidden" value=""/>
					<input name="oprid" type="hidden" value=""/>
					<input name="contact_id" type="hidden" value=""/>
					<input name="utm_source" type="hidden" value="<?=isset($_GET['utm_source']) ? $_GET['utm_source']:''?>"/>
					<input name="utm_medium" type="hidden" value="<?=isset($_GET['utm_medium']) ? $_GET['utm_medium']:''?>"/>
					<input name="utm_term" type="hidden" value=""/>
					<input name="utm_content" type="hidden" value="<?=isset($_GET['utm_content']) ? $_GET['utm_content']:''?>"/>
					<input name="utm_campaign" type="hidden" value="<?=isset($_GET['utm_campaign']) ? $_GET['utm_campaign']:''?>"/>
					<input name="referral_page" type="hidden" value=""/>
					<input name="uid" type="hidden" value="p2c9448f773"/>
					<input name="mopsbbk" type="hidden" value="72306761D47F11BBCFC2B2DC:110457A6F95D32C493F7C3FD"/>
				</form>
				<div class="form-thanks">
                    <h2>Поздравляем с умным решением! </h2>
                    <p>В ближайшее время мы свяжемся с вами 
                        и расскажем о рекламных возможностях.
                    </p>
                    <p>Мы работаем: ПН - ПТ, 9:00 - 18:00 (МСК)</p>
                </div>
			</div>
		</div>
		<!--Group-->
		<div class="modal modal__partners" id="regform_group">
			<div class="text">
				<span class="close-modal">x</span>
				<div class="modal__title">
					<div class="modal__title-text">
						Заберите групповую скидку на участие с друзьями
					</div>
				</div>
				<form class="form-noreload" action="https://forms.ontraport.com/v2.4/form_processor.php?" method="post" accept-charset="UTF-8">
					<p>Заполните форму, чтобы получить персональную скидку на билеты</p>
					<input name="firstname" type="text"  required value="" placeholder="Ваше имя" class="big-input"/>
					<input name="cell_phone" required type="text" value="" placeholder="Ваш телефон" class="big-input"/>
					<input name="company" type="text" placeholder="Сколько друзей берете?" class="big-input"/>
					<input name="utmspecial_428" type="hidden" value="<?=isset($_GET['utm_special']) ? $_GET['utm_special']:''?>"/>
					<input name="utmcampaig_224" type="hidden" value="<?=isset($_GET['utm_campaign']) ? $_GET['utm_campaign']:''?>"/>
					<input name="utmmedium_225" type="hidden" value="<?=isset($_GET['utm_medium']) ? $_GET['utm_medium']:''?>"/>
					<input name="utmsource_223" type="hidden" value="<?=isset($_GET['utm_source']) ? $_GET['utm_source']:''?>"/>
					<div class="moonray-form-element-wrapper moonray-form-element-wrapper-alignment-left moonray-form-input-type-submit">
						<button type="submit" name="submit-button" value="узнать о рекламных возможностях" class="btn-orange3" id="mr-field-element-584743536048"><span class="desktop-none">Забрать</span><span class="mobile-none">Получить групповую</span> скидку</button>
					</div>
					<input name="afft_" type="hidden" value=""/>
					<input name="aff_" type="hidden" value=""/>
					<input name="sess_" type="hidden" value=""/>
					<input name="ref_" type="hidden" value=""/>
					<input name="own_" type="hidden" value=""/>
					<input name="oprid" type="hidden" value=""/>
					<input name="contact_id" type="hidden" value=""/>
					<input name="utm_source" type="hidden" value="<?=isset($_GET['utm_source']) ? $_GET['utm_source']:''?>"/>
					<input name="utm_medium" type="hidden" value="<?=isset($_GET['utm_medium']) ? $_GET['utm_medium']:''?>"/>
					<input name="utm_term" type="hidden" value=""/>
					<input name="utm_content" type="hidden" value="<?=isset($_GET['utm_content']) ? $_GET['utm_content']:''?>"/>
					<input name="utm_campaign" type="hidden" value="<?=isset($_GET['utm_campaign']) ? $_GET['utm_campaign']:''?>"/>
					<input name="referral_page" type="hidden" value=""/>
					<input name="uid" type="hidden" value="p2c9448f773"/>
					<input name="mopsbbk" type="hidden" value="72306761D47F11BBCFC2B2DC:110457A6F95D32C493F7C3FD"/>
				</form>
				<div class="form-thanks">
                    <h2>Отлично!</h2>
                    <p>В ближайшее время мы свяжемся с вами 
                    и расскажем о рекламных возможностях. 
                    </p>
                    <p>Мы работаем: ПН - ПТ, 9:00 - 18:00 (МСК)</p>
                </div>
			</div>
		</div>
		<!--/modal forms-->
		<form style="display: none" id="payment-form" class="moonray-form-clearfix" action="https://forms.ontraport.com/v2.4/form_processor.php?" method="post" accept-charset="UTF-8">
			<input name="firstname" type="hidden" value="<?=isset($_GET['firstname'])?$_GET['firstname']:''?>" />
			<input name="email" type="hidden"  value="<?=isset($_GET['email'])?$_GET['email']:''?>" />
			<input name="cell_phone" required type="hidden" value="<?=isset($_GET['cell_phone'])?$_GET['cell_phone']:''?>" />
			<input name="utmspecial_428" type="hidden" value="<?=isset($_GET['utm_special'])?$_GET['utm_special']:''?>"/>
			<input name="utmcampaig_224" type="hidden" value="<?=isset($_GET['utm_campaign'])?$_GET['utm_campaign']:''?>"/>
			<input name="utmmedium_225" type="hidden" value="<?=isset($_GET['utm_medium'])?$_GET['']:'utm_medium'?>"/>
			<input name="utmsource_223" type="hidden" value="<?=isset($_GET['utm_source'])?$_GET['']:'utm_source'?>"/>
			<input type="submit" name="submit-button" value="Submit"/>
			<input name="afft_" type="hidden" value="<?=isset($_GET['afft_'])?$_GET['afft_']:''?>"/>
			<input name="aff_" type="hidden" value="<?=isset($_GET['aff_'])?$_GET['aff_']:''?>"/>
			<input name="sess_" type="hidden" value="<?=isset($_GET['sess_'])?$_GET['sess_']:''?>"/>
			<input name="ref_" type="hidden" value="<?=isset($_GET['ref_'])?$_GET['ref_']:''?>"/>
			<input name="own_" type="hidden" value="<?=isset($_GET['own_'])?$_GET['']:'own_'?>"/>
			<input name="oprid" type="hidden" value="<?=isset($_GET['oprid'])?$_GET['oprid']:''?>"/>
			<input name="contact_id" type="hidden" value="<?=isset($_GET['contact_id'])?$_GET['contact_id']:''?>"/>
			<input name="utm_source" type="hidden" value="<?=isset($_GET['utm_source'])?$_GET['']:'utm_source'?>"/>
			<input name="utm_medium" type="hidden" value="<?=isset($_GET['utm_medium'])?$_GET['utm_medium']:''?>"/>
			<input name="utm_term" type="hidden" value="<?=isset($_GET['utm_term'])?$_GET['']:'utm_term'?>"/>
			<input name="utm_content" type="hidden" value="<?=isset($_GET['utm_content'])?$_GET['utm_content']:''?>"/>
			<input name="utm_campaign" type="hidden" value="<?=isset($_GET['utm_campaign'])?$_GET['utm_campaign']:''?>"/>
			<input name="referral_page" type="hidden" value="<?=isset($_GET['referral_page'])?$_GET['referral_page']:''?>"/>
			<input name="uid" id="uid" type="hidden" value=""/>
			<input name="mopsbbk" id="mopsbbk" type="hidden" value=""/>
		</form>
		
		
		
		<script type="text/javascript" src="src/js/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="src/js/jquery-migrate-1.2.1.min.js"></script>
<!--Roistat-->
<script>(function(w, d, s, h, id) { w.roistatProjectId = id; w.roistatHost = h; var p = d.location.protocol == "https:" ? "https://" : "http://"; var u = /^.*roistat_visit=[^;]+(.*)?$/.test(d.cookie) ? "/dist/module.js" : "/api/site/1.0/"+id+"/init"; var js = d.createElement(s); js.charset="UTF-8"; js.async = 1; js.src = p+h+u; var js2 = d.getElementsByTagName(s)[0]; js2.parentNode.insertBefore(js, js2);})(window, document, 'script', 'cloud.roistat.com', '06bb8d989d90f9ec2816bc3debce9a5c');</script>
<!--end Roistat-->

    <script type="text/javascript" src="src/js/on-screen.min.js"></script>

    <script type="text/javascript" src="src/js/scrollreveal.min.js"></script>
    <script>
        $(document).ready(function(){

        $('.payment-link').click(function(e){
            e.preventDefault();
            $('#mopsbbk').val($(this).attr('data-mopsbbk'));
            $('#uid').val($(this).attr('data-uid'));

            $('#payment-form').submit();
        });
            
        //share
        // document.getElementById('vk_share').innerHTML = VK.Share.button('http://mysite.com', {type: 'link', text : ' '});
            //animate on scroll
            $('a[href^="#"]:not(".open-modal")').on('click', function() {
                var target = $(this).attr('href');
        var position = $(target).offset().top;
        var body = $("html, body");
        if(target == '#price') {
            position += 200;
        }
        if(target == '#uniqueness') {
            position -= 100;
        }
        body.stop().animate({scrollTop:position}, '1000');
        return false;
            });
        });
         //TODO: submit form
            $('.form-noreload').on('submit', function(e) {
                e.preventDefault();
                e.stopPropagation();
                
                $(this).slideUp().next('.form-thanks').slideDown();

                return false;
            });
        //animate crystals
        $('.thanks-header').onScreen({
            doIn: function() {
            $('.thanks-header').addClass('animated');
            }
        });
        $('.price-block').onScreen({
            doOut: function() {
            $('.thanks-header').removeClass('animated');
            }
        });
        $('.corp-offer__offer .crystal-1').onScreen({
            doIn: function() {
            $('.corp-offer__offer').addClass('animated');
            }
        });
        $('.corp-offer__offer').onScreen({
            doOut: function() {
            $('.corp-offer__offer').removeClass('animated');
            }
        });
        //timeline
        var timeline_items = $('.timeline li');
        var now_date = new Date();
        timeline_items.each(function() {
            var start_date = Date.parse( $(this).data('time-start') );
            var end_date = Date.parse($(this).data('time-end') );
            var date_diff1,date_diff2;
            if(end_date <= now_date) {
                if($(this).hasClass('timeline__line')) {
                    $(this).find('.timeline__line-red').css('width', '100%');
                } else if($(this).hasClass('timeline__dot')) {
                    $(this).addClass('dot-orange');
                }
            } else if(start_date < now_date) {
                if($(this).hasClass('timeline__line')) {
                    date_diff1 = now_date - start_date;
                    date_diff2 = end_date - start_date;
                    $(this).find('.timeline__line-red').css('width', (date_diff1*100)/date_diff2+'%');
                } else if($(this).hasClass('timeline__dot')) {
                    $(this).addClass('dot-orange');
                }
            } else {
            }
        });
        //fix menu
        $(window).on('load scroll', function() {
    var scrollPoint = 81;
    var $topNav = $('#topNav');
    if( ($(window).scrollTop() > scrollPoint) && !$topNav.hasClass('fixed') ) {
    $topNav.addClass('fixed');
    } else if( ($(window).scrollTop() < scrollPoint) && $topNav.hasClass('fixed') ) {
    $topNav.removeClass('fixed');
    }
        });
        //modal
        $('.open-modal').on('click', function() {
            var modal_id = $(this).attr('href')+'';
            $('#modalFon').show();
            $(modal_id).css('top', (($(window).scrollTop() + 50) + 'px') );
            $(modal_id).show();
            return false;
        });
        $('.close-modal').on('click', function() {
            $('#modalFon').hide();
            $(this).parents('.modal').hide();
            return false;
        });
        // $('.checkbox__label').click(function() {
                //  alert('Без согласия на обработку данных мы не можем принять заявку!');
        // });
        //show blocks on scroll
        ScrollReveal().reveal('.scrollreveal');
        $('.menu-icon').click(function() {
            $('body').toggleClass('menu-open');
        });
        $('.phone-icon').click(function() {
            $('body').toggleClass('phone-open');
        });
    </script>
		<!-- Yandex.Metrika counter -->
		<script type="text/javascript" async>
		(function (d, w, c) {
		(w[c] = w[c] || []).push(function() {
		try {
		w.yaCounter39253290 = new Ya.Metrika2({
		id:39253290,
		clickmap:true,
		trackLinks:true,
		accurateTrackBounce:true,
		webvisor:true
		});
		} catch(e) { }
		});
		var n = d.getElementsByTagName("script")[0],
		s = d.createElement("script"),
		f = function () { n.parentNode.insertBefore(s, n); };
		s.type = "text/javascript";
		s.async = true;
		s.src = "https://mc.yandex.ru/metrika/tag.js";
		if (w.opera == "[object Opera]") {
		d.addEventListener("DOMContentLoaded", f, false);
		} else { f(); }
		})(document, window, "yandex_metrika_callbacks2");
		</script>
		<noscript>
		<div><img src="https://mc.yandex.ru/watch/39253290" style="position:absolute;
			left:-9999px;" alt="" />
		</div></noscript>
		<!-- /Yandex.Metrika counter -->
	</body>
</html>